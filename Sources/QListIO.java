import java.io.*;
public class QListIO {//writes and  reads QLists
    private QListIO() {//The class cannot be either intstantiated or extended
    }
    public static void writeQList(QList q, File f) throws IOException {//writes a QList to a given file. It throws an Exception
        FileOutputStream fos=new FileOutputStream(f);//make a new FileOutputStream that wil write to the file represented by the parameter
        ObjectOutputStream oos=new ObjectOutputStream(fos);//
        oos.writeObject(q);
        oos.close();
    }
    public static QList readQList(File f) throws IOException, ClassNotFoundException {//read a QList from a file. It throws an Exception
        QList r=null;//the return variable: default to null
        FileInputStream fis=new FileInputStream(f);//create a new input stream that reads from the file parameter
        ObjectInputStream ois=new ObjectInputStream(fis);//create a new input stream that translates read BYTES int objects
        r=(QList) ois.readObject();//read the list from the file
        ois.close();//close all the streams
        return r;
    }
}