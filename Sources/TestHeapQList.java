import quizcards.*;
import java.io.File;
public class TestHeapQList {
    public static void main(String[] args) {
	System.out.println("Zeroth test of addCard()");
	testAddCard0();
	System.out.println("First test of addCard");
	testAddCard1();
	System.out.println("Second test of addCard");
	testAddCard2();
	System.out.println("Zeroth test of changeMin()");
	testChangeMin0();
	System.out.println("First test of changeMin()");
	testChangeMin1();
	System.out.println("Zeroth test of setActive(int i)");
	testSetActive();
	System.out.println("Zeroth test of setPassive(int i)");
	testSetPassive();
	System.out.println("Zeroth test of new HeapQList(QList q)");
	testNewOfQList();
	System.out.println("First test of new HeapQList(QList q)");
	testNewOfQList1();
	System.out.println("Test that new HeapQList(QList q) is repeatable.");
	testNewOfQListRepeatable();
	System.out.println("Zeroth test of getSourceFile()");
	testGetSourceFile();
    }
    public static void verifyHeap(HeapQList q) {
	System.out.println("List size: " + q.size());
	for(int i=0;child(i, 1) <= q.lastActiveIndex(); i++) {
	    Card parent = q.getCard(i);
	    for(int j=0;j<2;j++) {
		int childI=child(i, j);
		Card child = q.getCard(childI);
		if(child.getDueTurn() < parent.getDueTurn()) {
		    System.err.print("The card, " + child.getQuestion() + " " + child.getAnswer());
		    System.err.print(", at index " + childI);
		    System.err.print(" had a lower due value, " + child.getDueTurn());
		    System.err.print(", than that of its parent, " + parent.getQuestion() + " " + parent.getAnswer());
		    System.err.print(", at " + i);
		    System.err.print(", " + parent.getDueTurn()); 
		    System.err.println();
		}
	    }
	}
    }
    private static int child(int i, int child) {//child is the 0 for the left branch and 1 for the right branch
	return 2*i + 1 + child;
    }
    public static void testAddCard0() {
	HeapQList q=new HeapQList();
	for(int due=10;due >= 0;due--) {
	    Card c=new Card("a", "b", true);
	    c.setDueTurn(due);
	    q.addCard(c);
	    //the first card in the list should equal the card just added because we are always adding a card with a smaller due value than all the preceding cards
	    if(q.getCard(0) != c) {
		System.err.print("The " + (11 - due) + "th card added to the list was not moved to the beginning of the list, ");
		System.err.print("but was placed at index " + q.indexOf(c) + ".  ");
		System.err.println("The card at index 0 had due value " + q.getCard(0).getDueTurn() + ".");
	    }
	}
    }
    public static void testAddCard1() {
	HeapQList q=new HeapQList();
	for(int i=10;i >= 0;i--) {
	    Card c=new Card("a", "b", true);
	    c.setDueTurn(i);
	    q.addCard(c);
	}
	Card c=new Card("a", "b", true);
	Card d=new Card("a", "b", true);
	c.setDue(2.75);
	d.setDue(4.15);
	q.addCard(c);
	q.addCard(d);
	verifyHeap(q);
    }
    public static HeapQList makeAllActive() {
	HeapQList q=new HeapQList();
	for(int i=0;i < 11;i++) {
	    Card c=new Card("a" + i, "b" + i, true);
	    c.setDueTurn(i);
	    c.setFrequency(1.0/6);
	    q.addCard(c);
	}
	return q;
    }
    public static void testAddCard2() {
	HeapQList q = makeAllActive();
	Card c=new Card("a", "b", true);
	Card d=new Card("a", "b", true);
	c.setDueTurn(3);
	d.setDueTurn(4);
	q.addCard(c);
	q.addCard(d);
	verifyHeap(q);
    }
    public static void testChangeMin0() {
	HeapQList q = makeAllActive();
	Card oldMin=q.getCard(0);
	q.changeMin();
	System.err.println("New due value of old minimum card: " + oldMin.getDueTurn());
	verifyHeap(q);
    }
    public static HeapQList makeActivePassive() {
	HeapQList q=new HeapQList();
	for(int i=0;i < 11; i++) {
	    Card c=new Card("a" + i, "b" + i, i < 6);
	    c.setDueTurn(i);
	    c.setFrequency(1.0/6);
	    q.addCard(c);
	}
	return q;
    }
    public static void testChangeMin1() {
	HeapQList q = makeActivePassive();
	for(int i=q.lastActiveIndex() + 1; i < q.size();i++) {
	    if(!!q.getCard(i).getActive()) {
		System.err.println("The passive card at " + i + " was not passive");
	    }
	}
	int oldLastActive=q.lastActiveIndex();
	q.changeMin();
	if(oldLastActive != q.lastActiveIndex()) {
	    System.err.println("The last active index changed from " + oldLastActive + " to " + q.lastActiveIndex());
	}
	for(int i=q.lastActiveIndex() + 1; i < q.size();i++) {
	    if(!!q.getCard(i).getActive()) {
		System.err.println("The passive card at " + i + " was not passive");
	    }
	}
    }
    public static void testSetActive() {
	HeapQList q=makeActivePassive();
	double oldMaxDueTurn = q.getCard(0).getDueTurn();
	int firstPassive = q.lastActiveIndex() + 1;
	Card c = q.getCard(firstPassive);
	q.setActive(firstPassive);
	if(c.getDueTurn() != oldMaxDueTurn + 1) {
	    System.err.println("The new active card's due value was not set to 1 plus the due value of the old root card");
	}
	verifyHeap(q);//verify the heap
    }
    public static void testSetPassive() {
	HeapQList q=makeActivePassive();
	try {
	    q.setPassive(0);
	} catch (IndexOutOfBoundsException e) {
	    String throwerName = e.getStackTrace()[0].getMethodName();
	    System.out.println(throwerName);
	}
	verifyHeap(q);
    }
    public static void testNewOfQList() {
	QList q = QList.readQList(new File("../SavedQLists/StateCapitals2010.ql"));
	HeapQList hq = new HeapQList(q);
	if(q.size() != hq.size()) {
	    String firstPart = "The size of the original QList, " + q.size(); 
	    String secondPart = ", and the size of the HeapQList, " + hq.size() + ", were different.";
	    System.err.println(firstPart + secondPart);
	}
	if(q.lastActiveIndex() != hq.lastActiveIndex()) {
	    String firstPart = "The number of active cards in the original QList, " + q.lastActiveIndex();
	    String secondPart = ", and the number of active cards in the HeapQList, " + hq.lastActiveIndex() + ", were different.";
	    System.err.println(firstPart + secondPart);
	}
	verifyHeap(hq);
    }
    public static HeapQList testNewOfQList1() {
	QList q = QList.readQList(new File("../SavedQLists/StateCapitals2010.ql"));
	Card[] cardsList = new Card[q.lastActiveIndex() + 1];
	double[] oldDueValues = new double[q.lastActiveIndex() + 1];
	for(int i=0;i <= q.lastActiveIndex(); i++) {
	    cardsList[i] = q.getCard(i);
	    oldDueValues[i] = cardsList[i].getDue();
	}
	HeapQList hq=new HeapQList(q);
	verifyHeap(hq);
	for(int i=0; i < cardsList.length; i++) {
	    if(cardsList[i].getDue() != oldDueValues[i]) {
		System.err.print("The due value of the card at index " + i);
		System.err.println(" in the original QList changed from " + oldDueValues[i]);
	    }
	}
	return hq;
    }
    public static void testNewOfQListRepeatable() {
	java.util.ArrayList<Card> cardList = TestProctor.cardList();
	QList q = new QList(cardList);
	HeapQList hq = new HeapQList(q);
	testHeapQListRepeatable(hq, q, 6);
    }
    /* There seems to be a strange bug in HeapQList(QList q) that makes it a non-repeatable function, 
       probably having to do with the changing of due values.  This method basically checks whether it is repeatable. */
    public static void testHeapQListRepeatable(QList stabile, QList q, int c) {
	for(int i=0; i < c; i++) {//c times repeat
	    HeapQList hq = new HeapQList(q);
	    verifyQListsEqual(stabile, hq);
	}
    }
    /*tests if the two QLists are equal.  Should override equals() in QList for this, but that didn't work.*/
    private static void verifyQListsEqual(QList q, QList r) {
	if(q.size() != r.size()) {
	    System.err.println("The two QLists' sizes, " + q.size() + " and " + r.size() + ", were not equal.");
	    return;
	}
	boolean ret = true;
	for(int i = 0; i < q.size();i++) {
	    boolean b = TestCard.verifyEqual(q.getCard(i), r.getCard(i));
	    if(!b) {
		System.err.println("The cards, " + q.getCard(i) + " and " + r.getCard(i) + ", at index " + i + " were not equal.");
	    }
	    ret = b && ret;
	}
	System.out.println(ret);
	if(!ret) {
	    System.err.println("The two QLists were not equal.");
	}
    }
    public static void testGetSourceFile() {
	HeapQList hq = new HeapQList();
	try {
	    File f = hq.getSourceFile();
	} catch(NullPointerException ex) {
	    System.err.println("getSourceFile() threw a null pointer exception with a null source file");
	}
	hq = new HeapQList(QList.readQList(new File("../SavedQLists/StateCapitals2010.ql")));
	File f = hq.getSourceFile();
	if(!f.getPath().endsWith(".hql")) {
	    System.err.println("The file name did not end with '.hql'");
	}
    }
}
