package quizcards;//this classes package
import java.util.*;//import package that holds ArrayList
import java.io.*;//import package that holds classes that read from the file
public class QList implements Serializable{//A list to hold Cards
    
    private ArrayList<Card> list=new ArrayList<Card>();//holds all the cards in this QList
    private int lastActive=-1;//the index of the last active card in the list
    protected File sourceFile;//the file this QList was read from (null if new)
    static final long serialVersionUID = -155562497810138601L;//class version number
    
    public QList() {//make an empty QList, with an empty list
    }
    public QList(ArrayList<Card> cards)  {//one arg constructor: a list of cards
        /*the following is done so that passive and active cards (and the appropriate index) will be in the right place.*/
        for(Card i: cards) {//for each card in the ArrayList parameter
            addCard(i);//add it to the list of the QList
        }
    
    }
    
    public void addCard(Card c) {//add a card to this QList
        if(c.getActive()) {//if this card is active
            list.add(lastActive + 1, c);//add it after the last active card (first if none in the list
            lastActive++;//increment the index of the last active card
        } else {
            list.add(c);//add this card at the end of the list
        }
    }
    public Card getCard(int i) {//get the card at the specified index
        return list.get(i);//return the card at the specified index of the list
    }
    public void setActive(int i) {//set the card at the specified index of passive cards to active
        Card c=getCard(i);//get the passive card at the specified index
        if(!c.getActive()) {//if the card is not already active
            c.setActive(true);//set it active
            if(i!=(lastActive + 1)) {//i is NOT the index of the first passive card
                Card d=list.set(lastActive + 1, c);//set the first passive card to this Card and get the card that was there
                list.set(i, d);//set the original index of the moved card to the formerly first passive card
            }
            lastActive++;//increment the index of the lastActivecard
        } else {
	    throw new IndexOutOfBoundsException("The index you specified was one of a card that was already active");
	}
    }
    public void setPassive(int i) {//set the card at the specified index of active cards to passive
        Card c=getCard(i);//get the card at the specified index
        if(c.getActive()) {//if the card is not already passive
            c.setActive(false);//set it passive
            Card d=list.set(lastActive, c);//set the last active card to this card and get the card that was there
            lastActive--;//increment the lastActive variable
            list.set(i, d);//set the original index of the moved card to the formerly first active card
        } else {
	    throw new IndexOutOfBoundsException("The index you specified was one of a card that was already passive");
	}
    }
    public void setActive(int i, boolean act) {//set the card at the specified index to active or passive
        if(act) {//if the user wants to make the card active
            setActive(i);//call the setActive method for the index specified in the parameter
        } else {//if the user wants to make the card passive
            setPassive(i);//call the setPassive method for the index specified in the the parameter
        }
    }
    public int size() {//get the size of this list
        return list.size();
    }
    public int lastActiveIndex() {//gets the index of the last active card
        return lastActive;
    }
    public int indexOf(Card c) {
        return list.indexOf(c);
    }
    public void removeCard(int index) {//removes a card from the list
        list.remove(index);//remove from the list the card at the specified index
    }
    public void clear() {//deletes every element from the list
        list.clear();
    }
    /*Sets all of the cards in this QList to the ExponentialCard version of them. Destructive. */
    public void makeExponential() {
	for(int i=0;i<size();i++) {
	    ExponentialCard ec = list.get(i).asExponentialCard();
	    list.set(i, ec);
	}
    }
    ArrayList<Card> getList() {
	return list;
    }
    public String toString() {//a string representation of this QList
        return list.toString();
    }
    public File getSourceFile() {//sets the file this QList was read from
        return sourceFile;//return the source file of this QList
    }
    public static QList readQList(File f) {
        QList r=null;//the return variable: default to null
        try {//io can throw exceptions
            FileInputStream fis=new FileInputStream(f);//create a new input stream that reads from the file parameter
            ObjectInputStream ois=new ObjectInputStream(fis);//create a new input stream that translates read BYTES into objects
            r=(QList) ois.readObject();//read the list from the file
            ois.close();//close all the streams
	    r.sourceFile=f;//set the source File of the QList we just read to the file we just read it from
        } catch (IOException ex) {//catch any (hopefully not) thrown exceptions
            ex.printStackTrace();//print the exceptions stack trace
            System.out.println("Failed to load the file");
        } catch (ClassNotFoundException ex) {//catch the exception thrown if the class of the read object is not found
            ex.printStackTrace();//print the exceptions stack trace
        }
        return r;//return the aforementioned return value
    }
    public void writeQList(File f) {//write this QList to the given file
        try {//IO can throw Exceptions
            FileOutputStream fos=new FileOutputStream(f);//make a new output stream that writes bytes to the file parameter
            ObjectOutputStream oos=new ObjectOutputStream(fos);//make a new output stream that write Objects to bytes
            oos.writeObject(this);//write the current object
        } catch (IOException ex) {//catch the exception thrown
            ex.printStackTrace();
        }
    }
}
