package quizcards;//this classes package
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

public class QuizCardBuilder {
    private JTextArea question;//text area where the user types this card's question
    private JTextArea answer;//text area where the user types this card's answer
    private JTextField freq;//the text field for the frequency value
    private JTextField exp;//the text field for the exponent value
    private JButton nextButton = new JButton("Next Card");//make the button that goes to the next card
    private double defFreq=1.0 / 6.0;//the default frequency value
    private double defExp = .5;//the default exponent value
    private HeapQList cardList;
    private JFrame frame;
    
    // additional, bonus method not found in any book!

    public static void main (String[] args) {
       QuizCardBuilder builder = new QuizCardBuilder();
    }
    /*CHANGE to let the user close without saving*/
    /*                                           */
    public QuizCardBuilder() {
        go();
    }
    
    public void go() {
        // build gui
        frame = new JFrame("Quiz Card Builder"); // title bar
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

        JPanel numValues=new JPanel();//make a panel for the text fields for the default numerical data
        
        Box lBox=Box.createVerticalBox();//make a box for the labels for the text fields
        JLabel freqL=new JLabel("Frequency:");//label for the frequency field
	JLabel expL =new JLabel("Exponent:");//label for the exponent field
        lBox.add(freqL);//add the frequency label to this box
	lBox.add(expL);
        numValues.add(lBox);//add the label box to the numValues panel

        Box fBox=Box.createVerticalBox();//make a box for the text fields to go in
        freq=new JTextField(String.valueOf(defFreq), 4);//make the frequency text field
	exp =new JTextField(String.valueOf(defExp), 4);
        fBox.add(freq);//add the frequency text field to the box
	fBox.add(exp);
        numValues.add(fBox);//add the text-field box to the the panel
        
        JLabel top=new JLabel("Default numeric card values");//label for this frame
        JButton startCards=new JButton("Start making cards");//make a button to go out of the default-values screen
        startCards.addActionListener(new StartCardsListener());//add an action listener to the start cards method
        
        /* un-used menuJMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem newMenuItem = new JMenuItem("New");
        JMenuItem saveMenuItem = new JMenuItem("Save");
        fileMenu.add(newMenuItem);
        menuBar.add(fileMenu); */

        frame.getContentPane().add(numValues, BorderLayout.CENTER);//add the JFanel with the labels and text fields to the frame
        frame.getContentPane().add(top, BorderLayout.NORTH);//add the title label to the frame
        frame.getContentPane().add(startCards, BorderLayout.SOUTH);//add the end-screen button to the frame
        frame.setSize(205, 175);//set the size of the frame
        frame.setVisible(true);//set the frame to visible
    }

    public void setUpMainGui() {
        frame.getContentPane().removeAll();//remove all the components from the frame (some will be re-added)
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new CloseWindowListener());//register window actions with the frame
        JPanel mainPanel = new JPanel();
        JLabel qLabel = new JLabel("Question:");
        
        Font bigFont = new Font("sanserif", Font.BOLD, 24);
        question = new JTextArea(6,20);
        question.setLineWrap(true);
        question.setWrapStyleWord(true);
        question.setFont(bigFont);
       
        JScrollPane qScroller = new JScrollPane(question);
        qScroller.setVerticalScrollBarPolicy(
                  ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        qScroller.setHorizontalScrollBarPolicy(
                  ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JLabel aLabel = new JLabel("Answer:");
        answer = new JTextArea(6,20);
        answer.setLineWrap(true);
        answer.setWrapStyleWord(true);
        answer.setFont(bigFont);
       
        JScrollPane aScroller = new JScrollPane(answer);
        aScroller.setVerticalScrollBarPolicy(
                  ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        aScroller.setHorizontalScrollBarPolicy(
                  ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        
        JLabel freqL=new JLabel("Frequency:");//label for the frequency field
	JLabel expL =new JLabel("Exponent:");//label for the exponent field
	JPanel fieldPanel=new JPanel();//make a new (default) JPanel for the text fields
	fieldPanel.add(freqL);
        fieldPanel.add(freq);//add the frequency text field to the text field panel
	fieldPanel.add(expL);
	fieldPanel.add(exp);
        
        mainPanel.add(qLabel);//add the question label to the main panel
        mainPanel.add(qScroller);//add the question scrollpanne (with the text area in it) to the main panel
        mainPanel.add(aLabel);//add the answer label to the main panel
        mainPanel.add(aScroller);//add the answer scroll pane (with the text area in it) to the main panel
        mainPanel.add(fieldPanel);//add the panel with the text fields to the main panel
        
        nextButton.addActionListener(new NextCardListener());//make an action listener and give it to the button
        JButton closeButton = new JButton("Save QList");//saves the program
        closeButton.addActionListener(new CloseButtonListener());//listener for the quit button
        /*make a new panel for everything that goes in the south of the frame*/
        JPanel buttonPanel=new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));//set its layout
        buttonPanel.add(nextButton);//add the button for the next card on it to the south panel
        buttonPanel.add(closeButton);//add the quit button to the south panel
        
        frame.getContentPane().add(BorderLayout.SOUTH, buttonPanel);
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        frame.setSize(530, 550);//set the size of the frame
        cardList = new HeapQList();//set the cardlist to a new (empty) QList
	question.requestFocus();
    }

    public class StartCardsListener implements ActionListener {//listener class for the start cards button
        public void actionPerformed(ActionEvent e) {//method for the interface this implements
            try {//this can throw (unchecked) exceptions
                double freqVal=getNumericValue(freq);//parse the text in the frequency field to double 
		double exponent=getNumericValue(exp);
                Card.validateFrequency(freqVal);//see whether this frequency value from the GUI is a valid frequency
		ExponentialCard.validateExponent(exponent);
                defFreq=freqVal;//set the default frequency to the user-inputed frequency
		defExp =exponent;
                setUpMainGui();//set up the main gui
	    } catch (NumberFormatException ex) {
		//do nothing, because this exception was already handled by the getNumericValue method.
		/*This catch block is here only because otherwise the following block would run, because 
		  NumberFormatException extends IllegalArgumentException.*/
            } catch (IllegalArgumentException ex) {//thrown if any of the numeric parameters was invalid
		JOptionPane.showMessageDialog(frame, ex.getMessage(), "Value error", JOptionPane.ERROR_MESSAGE);//Show a dialog that tells the user about his error
            }
        }
    }

    public class NextCardListener implements ActionListener {
        public void actionPerformed(ActionEvent ev) {
            try {//these statements can throw (unchecked) exceptions I want to catch
                /*get the contents of the three text fields and translate them to numbers*/
                double freqNum=getNumericValue(freq);//for the frequency text field
		double expNum =getNumericValue(exp);
                /*make a new card from the data on the gui (or not if an exception was thrown)*/
                ExponentialCard card = new ExponentialCard(question.getText(), answer.getText(), false, freqNum, expNum);
                cardList.addCard(card);//add the card to the QList
                clearCard();//clear all the components
	    } catch (NumberFormatException ex) {
		//do nothing, because this exception was already handled by the getNumericValue method.
		/*This catch block is here only because otherwise the following block would run, because 
		  NumberFormatException extends IllegalArgumentException.*/
            } catch (IllegalArgumentException e) {//catch the exception thrown when the numeric values were invalid for the card
		JOptionPane.showMessageDialog(frame, e.getMessage(), "Value error", JOptionPane.ERROR_MESSAGE);//Show a dialog that tells the user about his error
            }
        }
    }

    /* Gets the numeric value of a text field.  Catches the exception and shows a message if the value 
       doesn't parse as a number, but then rethrows the exception assuming that execution needed to be halted to give 
       the user a change to change the value. */
    private double getNumericValue(JTextField field) {
	String text = field.getText();
	try {
	    return Double.parseDouble(text);
	} catch (NumberFormatException ex) {
	    String variableName = "";
	    if(field == freq) {
		variableName = "frequency";
	    } else if(field == exp) {
		variableName = "exponent";
	    }
	    JOptionPane.showMessageDialog(frame, "The value you entered for the " + variableName + ", \"" + text
		                                  + "\", was not a number.", "Parse Error", JOptionPane.ERROR_MESSAGE);
	    field.selectAll();
	    field.requestFocus();
	    throw ex;//rethrow the exception we just caught so that it stops the double being set
	}
    }

    private boolean saveQList() {
	//if both the question and answer fields are empty
	if(!question.getText().isEmpty() && !answer.getText().isEmpty()) {
	    nextButton.doClick();//use software to click the button (if will make and add the current card to the deck)
	}
        JFileChooser loc=new JFileChooser();//make a gui file chooser
        loc.setSelectedFile(new File("FirstQList.hql"));//give the user a default file
        int x=loc.showSaveDialog(frame);//show the dialog so the user can choose which file to save it to
        boolean r=(x!=JFileChooser.CANCEL_OPTION);//set a return value to whether the user did not press the cancel button on the dialog
        if (r) {//if the user did NOT press the button
            File f=loc.getSelectedFile();//get the file the user selected
            cardList.writeQList(f);//write the QList
        }//else do nothing
        return r;//return the return value
    }
    
    public class CloseWindowListener implements WindowListener {//listens for things the window does
        public void windowClosed(WindowEvent e) {//empty method inserted to fulfill WindowListener contract
        }
        public void windowDeactivated(WindowEvent e) {//empty method inserted to fulfill WindowListener contract
        }
        public void windowActivated(WindowEvent e) {//empty method inserted to fulfill WindowListener contract
        }
        public void windowDeiconified(WindowEvent e) {//empty method inserted to fulfill WindowListener contract
        }
        public void windowIconified(WindowEvent e) {//empty method inserted to fulfill WindowListener contract
        }
        public void windowClosing(WindowEvent e) {//called when the window is closing
	    //find out whether the list, question area, and answer area are empty 
	    //so that we know we don't need to save anything
	    boolean lEmpty = cardList.size() == 0;
	    boolean qEmpty = question.getText().length() == 0;
	    boolean aEmpty = answer.getText().length() == 0;
	    boolean empty = lEmpty && qEmpty && aEmpty;
	    int r = 0;//a variable for whether to exit or not that will later be set to the return on an option pane
	    if(!empty) {
		String m = "Do you want to save the list of cards that you've made?";
		int o =  JOptionPane.YES_NO_CANCEL_OPTION;
		int i = JOptionPane.QUESTION_MESSAGE;
		String[] bs = { "Save", "Discard", "Cancel quit"};
		r = JOptionPane.showOptionDialog(frame, m, "Save QList?", o, i, null, bs, 0);
		if(r == JOptionPane.YES_OPTION) {
		    saveQList();//save the QList (or not if the user cancels)
		}
	    }
            if(!(r==JOptionPane.CLOSED_OPTION || r == JOptionPane.CANCEL_OPTION)) {//if the user did NOT cancel
                System.exit(0);//exit the program
            }
        }
        public void windowOpened(WindowEvent e) {//empty method inserted to fulfill WindowListener contract
        }
    }
    
    public class CloseButtonListener implements ActionListener {//listener class for the close button
        public void actionPerformed(ActionEvent e) {//method for the interface this implements
            boolean c=saveQList();//save the QList (or not if the user cancelled)
            if(c) {//if the user DID NOT cancel the saving
                /*make the properties of an option dialog*/
                String title="Do you want to quit?";//make the title of the dialog
                String message="Do you want to quit the program or start another deck of cards?";//make the message the dialog will display
                String[] options={"Quit", "Start another deck"};//make the labels that will go on the option buttons
                /*make the dialog*/
                int r=JOptionPane.showOptionDialog(frame, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                if(r==JOptionPane.YES_OPTION) {//if the user wanted to quit
                    System.exit(0);//exit the program
                } else {//whatever else he click (Start another deck or the x in the corner)
                    cardList.clear();//clear the list of cards for a whole new deck
                    clearCard();//clear the text fields for another card
                }//end else
            }//end higher if
        }//end method
    }//end inner class
    private void clearCard() {
       question.setText("");//clear the question text field
       answer.setText("");//clear the answer text field
       freq.setText(String.valueOf(defFreq));//set the text in the frequency text field to the default frequency
       exp.setText(String.valueOf(defExp));
       question.requestFocus();
    }
}
