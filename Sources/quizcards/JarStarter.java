package quizcards;//this classes package
import javax.swing.*;//import frame, buttons, etc.
import java.awt.event.*;//import Event classes
import java.awt.BorderLayout;//import the layout class
public class JarStarter {
    JFrame f;//the frame for this GUI

    public static void main(String[] args) {
        JarStarter j=new JarStarter();//make an new object of this type
        j.go();//call its instance starter method
    }
    public void go() {//Sets up the gui
        f=new JFrame("Quiz Card Program starter");//make a new frame to hold the gui for choosing with program to run
        JButton p=new JButton("Start Proctor");//make a JButton which will be used to start proctor
        JButton qcb=new JButton ("Start QuizCardBuilder");//make a JButton which will be used to start QuizCardBuilder
        p.addActionListener(new ProctorListener());//have one of the inner classes listen for when the proctor button is clicked
        qcb.addActionListener(new QuizCardBuilderListener());//have the other inner class listen for when the QuizCardBuilder button is clicked
        f.getContentPane().add(p, BorderLayout.NORTH);//add one button to the top of the frame
        f.getContentPane().add(qcb, BorderLayout.SOUTH);//add the other button to the bottom of the frame
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//set the frame to exitting the program when it is closed
        f.setBounds(300, 300, 335, 80);//set the size and location of the frame
        f.setVisible(true);//set the frame visible
    }
    
    public class ProctorListener implements ActionListener {//overides the listener class so it can listen to the proctor button
        public void actionPerformed(ActionEvent e) {//the listener method
            Display.main(new String[0]);//call Display's main method
            f.dispose();//dispose of this applications GUI
        }
    }
    
    public class QuizCardBuilderListener implements ActionListener {//implements the listener class so it can listen to the QuizCardBuilder button
        public void actionPerformed(ActionEvent e) {//the listener method
            QuizCardBuilder.main(new String[0]);//call the QuizCardBuilder main method\
            f.dispose();//dispose of this application's GUI
        }
    }
}
