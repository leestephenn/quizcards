package quizcards;
import java.util.*;
//I may eventually return to a Buffer class, which hold such things as number
//of cards, but this is smaller abstraction so I will use it first
/* This class represents a node of a linked list that is itself a linked list 
   of cards sorted by frequency. All the cards in it should have the same turn
   number */
public class BufferObject extends LinkedList<quizcards.Card> {
    /*LinkedList has more power than BufferObject really should have.  Adding 
      at an index in particular should be avoided because this object is sorted.
      I have overriden those methods to do throw RuntimeExceptions. */

    private static int bound;//the number of cards that should be in the buffer

    private BufferObject next;//the next card in the buffer

    /* Just implements the superclass contructor */
    public BufferObject() {
	super();
    }
    
    /* Implements the superclass constructor, which calls addAll(), which I 
       override. */
    public BufferObject(Collection<? extends Card> xs) {
	super(xs);
    }

    /* Constructs a BufferObject with a next link to the n parameter */
    public BufferObject(BufferObject n) {
	this();
	setNext(n);
    }

    /* Sets the buffer bound to the b parameter. Throws an exception if b is
       negative. */
    public static void setBound(int b) {
	if(b < 0) {
	    throw new IllegalArgumentException("The buffer's size must not be negative.");
	}
	bound = b;
    }

    /*Returns the buffer bound*/
    public static int bound() {
	return bound;
    }

    /*Adds the card to this list at the appropriate place in the order if it has
      the same turn number as this buffer*/
    public boolean add(Card aCard) {
	if(!isEmpty() && aCard.getDueTurn() != getDueTurn()) {
	    throw new IllegalArgumentException("The card already in the buffer object had a due turn of " +
		    getMin().getDueTurn() + " while the card you tried to add had a due turn of " + aCard.getDueTurn());
	}
	ListIterator<Card> it = this.listIterator();
	while(it.hasPrevious()) {
	    Card c = it.previous();//linked lists are, in fact, doubly linked
	    if(c.getFrequency() < aCard.getFrequency()) {
		it.add(aCard);//adding it to the iterator adds it to the list and increments the size of the list
		return true;
	    }
	}
	this.addFirst(aCard);
	return true;
    }

    /* Adds each card in the collection parameter at the right place in this 
       BufferObject */
    public boolean addAll(Collection<? extends Card> xs) {
	Card[] itsCards = xs.toArray(new Card[0]);
	//I can't sort the list first and then add it because there could be an element of the buffer that is between 
	//its cards.
	for(Card c : itsCards) {
	    add(c);
	}
	return true;
    }

    /* Called on the last BufferObject in the buffer. Sets the next BufferObject
       to a new BufferObject composed of all the cards in the the actives heap 
       with the next dueTurn number.  Calls itself on the new next BufferObject
       unless the bufferSize plus the number of cards added is greater than the
       bound. */
    public void addActiveCards(PriorityQueue<? extends Card> actives, int bufferSize) {
	BufferObject bo = new BufferObject();
	bo.fillFromActives(actives);
	setNext(bo);
	bufferSize = bufferSize + bo.size();
	if(bufferSize <= bound) {
	    bo.addActiveCards(actives, bufferSize);
	}
    }
    
    /* Adds all the cards in the active heap with the minimum turn number to the
       BufferObject on which it is called. */
    public void fillFromActives(PriorityQueue<? extends Card> actives) {
	Card min = actives.peek();
	if(isEmpty() || min.getDueTurn() == getDueTurn()) {
	    add(min);
	    actives.remove(min);
	    fillFromActives(actives);
	}
    }


    /* The following three methods are undefined because this object is sorted */

    public void add(int index, Card element) {
	throw new RuntimeException("Adding an element to a BufferObject at"
	       + " an index is not allowed because BufferObjects are sorted.");
    }

    public boolean addAll(int index, Collection<? extends Card> c) {
	throw new RuntimeException("Adding an element to a BufferObject at"
		+ " an index is not allowed because BufferObjects are sorted.");
    }
    
    public Card set(int index, Card element) {
	throw new RuntimeException("Adding an element to a BufferObject at"
		+ " an index is not allowed because BufferObjects are sorted.");
    }


    /* Equivalent to getFirst() and get(0), but emphasizes that this list is
       sorted by frequency. Functional (does not remove card from buffer).*/
    public Card getMin() {
	try {
	    return getFirst();
	} catch(NoSuchElementException ex) {
	    NoSuchElementException mine = new NoSuchElementException("The BufferObject is empty");
	    mine.initCause(ex);
	    throw mine;
	}
    }
    
    /* Returns the DueTurn number of all the cards in this buffer. Throws a 
       NoSuchElementException if this buffer is empty. */
    public int getDueTurn() {
	return getMin().getDueTurn();
    }

    /* Sets the next card in the buffer to the n parameter */
    public void setNext(BufferObject n) {
	if(n.isEmpty()) {
	    throw new IllegalArgumentException("The BufferObject you passed in to next() was empty.");
	}
	next = n;
    }

    /* If the next BufferObject has cards, returns it. If the next BufferObject
       is empty, removes it from the buffer and recurses. */
    public BufferObject next() {
	//while the (recursive) loop bellow deals with not returning empty 
	//buffer objects that are not first in the list, there is still the 
	//problem of an empty BufferObject that is first in the list.  This may
      	//be an argument for putting lookahead() in Proctor.
	if(next != null && next.isEmpty()) {
	    next = next.next();
	}
	return next;
    }
    
    /* Returns whether this is the last card in the buffer or not (whether 
       next() is null) */
    public boolean isLast() {
	return next == null;
    }

    //other than linked-list stuff which I get for free, the only other thing 
    //that Dad's pseudocode has is getFrequency, which acts in effect on the 
    //first card: If it is used enough, I might add it

    

    /* Takes the current turn number and returns the percent early of the card 
       in this BufferObject with minimum percent early. */
    public double percentEarly(int curTurn) {
	int earliness=getDueTurn() - curTurn;
	//because the earliness is the same for all the cards in this buffer, 
	//the card with the minimum percent early in this buffer will be the 
	//card with the minimum frequency, i.e., the first card.
	return earliness * getMin().getFrequency();
    }
}
