package quizcards;
import javax.swing.*;//contains gui classes (JFrame, JLabel, et al)
import java.awt.event.*;//contains event handling classes
import java.awt.BorderLayout;//import a class that contains constants for position of components on the frame
public class Display extends WindowAdapter {//class which displays the cards, extends this class for a gui fuction
    private Proctor p;//the proctor that decides which card is quizzed next
    private JFrame f;//holds all the graphics
    private JTextArea q;//shows the question
    private JLabel sumFDisplay=new JLabel();//shows the sum of frequencies when there are no more passive cards
    private JTextArea userA;//where the user can type his answer and his answer is displayed
    private JScrollPane scroller;//where the answer area goes
    private JButton showA;//button which the user clicks when he wants to be shown the answer
    private JTextArea a;//shows the correct answer
    private JButton know;//button which the user clicks if he knows the answer
    private JButton dontKnow;//button which the user clicks if he doesn't know the answer
    private JLabel sumFreqLabel=new JLabel();//label that contains the sum of frequencies when it is displayed

    public static void main(String[] args) {//main method that chooses a QList from a JFileChooser
	Display d=new Display();//make a new display
	d.openDeck();
    }
    
    public Display(Proctor aProctor) {
	this();//call the other, no-arg, constructor
	setProctor(aProctor);
    }
    public Display() {//constructor
	f=new JFrame("Quizing program");//construct the frame
	f.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	q=new JTextArea(5,20);//construct the text area for the question
	q.setLineWrap(true);//have the question line wrap
	q.setWrapStyleWord(true);//have it line wrap at word boundaries
	q.setEditable(false);//make it so the user can't edit the question
	sumFreqLabel.setVisible(false);//set the sumF display label to invisible
	JPanel north = new JPanel();
	north.add(q);
	north.add(sumFreqLabel);
	userA=new JTextArea(5, 20);//construct the text area for the user's answer
	userA.setLineWrap(true);// the text area line wrap
	userA.setWrapStyleWord(true);//have it line wrap at word boundaries
	scroller=new JScrollPane(userA);//put the text area in the scroll pane
	scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);//when the text is more than 4 lines, add a scrollbar
	scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);//never scroll horizontally
	showA=new JButton("Show answer");//construct the show answer button
	showA.addActionListener(new ShowListener());
	showA.setMnemonic(KeyEvent.VK_S);//add a key press that will activate the button
	a=new JTextArea(5, 20);//construct the text area for the correct answer
	a.setLineWrap(true);//have this text area line wrap
	a.setWrapStyleWord(true);//have it line wrap at word boundaries
	a.setEditable(false);//make it so the user can't edit the answer
	know=new JButton("I know it");//construct the button the user clicks if he knew the answer
	dontKnow=new JButton("I don't know it");//construct the button the user clicks if he did not know the answer
	NextListener aL=new NextListener();
	know.addActionListener(aL);//register it with the know button
	know.setMnemonic(KeyEvent.VK_K);//give the know button a key press that activates it
	dontKnow.addActionListener(aL);//register it with the don't know button
	dontKnow.setMnemonic(KeyEvent.VK_D);//give the dontKnow button a key press that clicks it
	f.getContentPane().add(north, BorderLayout.NORTH);//add the question text area to the frame
	//userA, showA, scroller, know and dontKnow are added later, in displayCard() and ShowListener 
	f.addWindowListener(this);//add this class as a window listener to the frame
	f.setBounds(300, 450, 300, 300);//set the size and location of the frame
	f.setVisible(true);
	sumFreqLabel.setVisible(false);//set the sumF display label to invisible
    }
    public void openDeck() {
        JFileChooser jfc=new JFileChooser();//make a new JFileChooser to choose the QList
        int r = jfc.showOpenDialog(f);//show the JFileChooser dialog
	if(r == JFileChooser.APPROVE_OPTION) {
	    QList q=QList.readQList(jfc.getSelectedFile());//Read the file the user chose and get its QList
	    if(q == null) {//if a QList could not be read out of the file
		java.io.File file = jfc.getSelectedFile();
		JOptionPane.showMessageDialog(f, 
			"Quiz cards could not be read out of " + file + ". Please select another file.",
			"File read error", JOptionPane.ERROR_MESSAGE);
		openDeck();
		return;
	    }
	    p=new Proctor(q);//make a new Proctor object with the QList we just got
	    if(p.allActive()) {
		updateSumFrequencies(p.getSumFrequency());
	    }
	    displayCard(p.nextCard());//start quizzing on that QList
	} else {
	    System.exit(0);
	}
    }
    public void setProctor(Proctor aProctor) {
	if(aProctor != null) {
	    p = aProctor;
	}
    }
    public void displayCard(Card c) {
	f.getContentPane().add(scroller, BorderLayout.CENTER);//add the scroller with the text area to the center of the frame
	f.getContentPane().add(showA, BorderLayout.SOUTH);//add the show answer button to the bottom of the frame
	f.validate();//show the difference
	displayQuestion(c.getQuestion());//display the card's question
	displayAnswer(c.getAnswer());//set the answer text field's text to the card's answer
	userA.requestFocus();//so the user can just type his answer
    }
    public void displayQuestion(String question) {//sets the text of the question text area to its parameter
	q.setText(question);//set the text of the question's text area to the parameter
    }
    public void displayAnswer(String answer) {//sets the text of the question text area to its parameter
	a.setText(answer);//set the text of the answer's text area to that of the parameter
    }
    public void updateSumFrequencies(double sumFreq) {
	String roundedSumFreq=new java.text.DecimalFormat("0.00").format(sumFreq);//format sumFreq to 2 decimal places
	sumFreqLabel.setText(roundedSumFreq);//update the label that displays the sum of frequencies
	if(!sumFreqLabel.isVisible()) {
	    q.setLineWrap(false);//turn of the question text area's line wrap so we can change its size
	    q.setColumns(17);//set the size of the the question to 3 less than its current size
	    sumFreqLabel.setVisible(true);//set the sum of Frequencies label visible
	    q.setLineWrap(true);//set line wrap back on
	}
    }
    public void hideSumFrequencies() {
	sumFreqLabel.setVisible(false);
	q.setLineWrap(false);
	q.setColumns(20);
	q.setLineWrap(true);
    }
    public void dispose() {//disposes of the frame
	f.dispose();//dispose the frame
    }
    public class ShowListener implements ActionListener {//shows the answer to the question
	public void actionPerformed(ActionEvent e) {
	    f.getContentPane().remove(showA);//remove the show answer button
	    userA.setEditable(false);//make the user answer area non-editable
	    JPanel center=new JPanel();//make a new JPanel for the stuff that will go in the center of the frame
	    center.add(scroller);//add the scroller to it
	    center.add(a);//add the text area with the corrrect answer to it
	    f.getContentPane().add(center, BorderLayout.CENTER);//add the panel to the center of the frame
	    JPanel knowledgeButtons=new JPanel();//panel for the know and don't know buttons
	    knowledgeButtons.add(know);//add the know button
	    knowledgeButtons.add(dontKnow);//add the don't know button
	    f.getContentPane().add(knowledgeButtons, BorderLayout.SOUTH);//add the panel to the frame
	    f.validate();//show all the things we just added
	}
    }
    public class NextListener implements ActionListener {//stores the students evaluation and shows the next cards
	public void actionPerformed(ActionEvent e) {
	    Object o=e.getSource();//get the source of the event
	    if(o==know) {//if it is the know button
		p.knewResponse();//respond to the user's correct answer
	    } else if (o==dontKnow) {//if the source of the event is the don't know button
		p.notKnewResponse();//respond to the user's correct answer
	    }
	    if(p.allActive()) {
		updateSumFrequencies(p.getSumFrequency());
	    }
	    if(sumFreqLabel.isVisible() && !p.allActive()) {
		hideSumFrequencies();
	    }
	    removeCorrectAnswer();
	    Card c=p.nextCard();//get the next card from proctor
	    displayCard(c);//display the card on the changed gui
	}//end method
    }//end inner class
    /* Removes the correct answer components from this display*/
    public void removeCorrectAnswer() {
        f.getContentPane().remove(a.getParent());//remove the correct answer from the frame
        know.getParent().setVisible(false);//set the know it and don't know it buttons invisible
        f.getContentPane().remove(know.getParent());//remove the know it and don't know it buttons
        f.validate();//update the frame sumFreq's size is changed HERE
        userA.setText("");//clear the user answer field
        userA.setEditable(true);//make the user answer area editable
    }
    public void windowClosing(WindowEvent e) {//window-listener method
	//BUG: Quits after hitting x in corner of quitting dialog
        int dR=-255;//the return from the dialog that asks the user whether he wants to see the correct answer or not
        boolean userAnswering=showA.isVisible();//a boolean for if they clicked the x in the corner
        if(userAnswering) {//if the frame is in the user-answer state
            /*make properties of the dialog to be shown*/
            String dMessage="Do you want to see the correct answer to that question before you quit?";//make the dialog's message
            String dTitle="Quit?";//make the dialog's title
            int dType=JOptionPane.YES_NO_OPTION;//the type dialog we want
            int dImage=JOptionPane.QUESTION_MESSAGE;//the index of the image of the dialog
            Object[] oa={"Show correct answer", "Quit"};//the labels on the buttons
            dR=JOptionPane.showOptionDialog(f, dMessage, dTitle, dType, dImage, null, oa, oa[0]);//show the dialog and get which button the user pressed
        } else {//if the frame is showing the correct answer
            String dMessage="Please say whether you know the answer to that question";//question the dialogs asks
            String dTitle="Did you know it";//the title of the dialog
            int dType=JOptionPane.YES_NO_CANCEL_OPTION;//the type of dialog we want
            int dImage=JOptionPane.PLAIN_MESSAGE;//the image the dialog displays
            Object[] oa={"I know it", "I don't know it", "Quit"};//make an array of the options for the dialog buttons
            dR=JOptionPane.showOptionDialog(f, dMessage, dTitle, dType, dImage, null, oa, oa[1]);//show the dialog and get which button the user pressed
        }
        closeDialogResponse(!userAnswering, dR);//call the method that responds the dialog
    }
    public void closeDialogResponse(boolean fromAnswer, int r) {//after the original closing dialog has been shown, this method is called with whether it was called from the answer frame and the int returned from the dialog
        if(!(r==JOptionPane.CLOSED_OPTION)) {//if the user did not click the x in the corner of the dialog
            if((!fromAnswer) && (r==JOptionPane.YES_OPTION)) {//if the user wanted to see the correct answer rather than quit
                showA.doClick();//show the correct answer by pressing that button
            } else {
                if(fromAnswer && r==JOptionPane.YES_OPTION) {//if the user said he knew the question
                    know.doClick();//respond as he answered the question correctly
                }
                if (fromAnswer && r==JOptionPane.NO_OPTION) {//if the user said he did not know the question
                    dontKnow.doClick();//respond as he answered incorrectly
                }
                saveQList();//save the QList or not
                /*make the properties of a dialog*/
                String dMessage="Do you want to quit or start a new deck?";//the message of the dialog
                int dType=JOptionPane.YES_NO_OPTION;//the type of the dialog
                int dImage=JOptionPane.QUESTION_MESSAGE;//the index of the image of the dialog
                Object[] dBtitles={"Quit?", "Open new deck"};
                int bN=JOptionPane.showOptionDialog(f, dMessage, "Quit?", dType, dImage, null, dBtitles, null);//show a dialog asking the user whether he wants to quit or open a new deck
                if (bN==JOptionPane.YES_OPTION) {//if the user wanted to quit
                    System.exit(0);//exit the program
                }
                if (bN==JOptionPane.NO_OPTION) {//if the user wanted start another deck
                    //JPanel p=new JPanel(new BorderLayout());//make a new panel to remake the frame's content's on
                    if(fromAnswer && r==JOptionPane.CANCEL_OPTION) {//if the user pressed the quit button rather than either of the know-it buttons
                        removeCorrectAnswer();//remove the correct answer from the frame
                    }
                    openDeck();//open a deck with this class as the base display
		    //BUG: exits display at this point (or rather, in openDeck)
                }
            } 
        }
    }
    public boolean saveQList() {
        JFileChooser sFC=new JFileChooser();//make a file chooser which chooses the file the QList was opened from by default
	QList list = p.getQList();
        sFC.setSelectedFile(list.getSourceFile());//set the default selected file of the file chooser to the file the QList was read from
        int r=sFC.showSaveDialog(f);//show the save dialog
        if(r==JFileChooser.APPROVE_OPTION) {//if the user chose to save the file
            list.writeQList(sFC.getSelectedFile());//write the QList to the file the user selected
        }
        return r==JFileChooser.APPROVE_OPTION;//return whether the user approved the save
    }
}
