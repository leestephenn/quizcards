package quizcards;
import java.util.*;
public class LookaheadProctor { // extends Proctor?
    //before declaring this finished, make sure to look at revision 6de45ce681e7
    //to check that this is using turnNumber correctly

    //the heap, sorted so that the minimum frequency is at the root, that holds all the cards whose dueTurns are less
    //than the current turn
    private PriorityQueue<Card> dueCards = new PriorityQueue<Card>(11, dueHeapSorter());
    //the first element of the linked list (see BufferObject for details) that holds cards that are nearly due
    private BufferObject buffer1 = new BufferObject();
    //the heap, sorted so that the minimum turn number is at the root, that holds the active cards not in the buffer or due heap
    private PriorityQueue<Card> actives =  new PriorityQueue<Card>(11, activeHeapSorter());
    private ArrayList<Card> passives = new ArrayList<Card>(); //the passive cards
    private Card current;//the card currently being quizzed on
    int currentTurn;//the current turn number

    /*public LookaheadProctor(QList q) {
	dueCards = new PriorityQueue<Card>(11, dueHeapSorter);
    }*/

    public Card nextCard() {
	Card ret = null;//the card to be returned
	addDueCardsFromBuffer();
	addDueCardsFromHeap();
	if(!dueCards.isEmpty()) {
	    ret = dueCards.poll();//poll() retrieves and removes the head of the queue
	} else {
	    if(!passives.isEmpty()) {
		ret = lookahead();
		if(ret==null) {
		    ret = passives.get(0);
		    ret.setActive(true);
		    passives.remove(ret);
		}
	    } else {
		ret = leastEarly();
	    }
	}
	return ret;
    }

    /* Puts all the due cards in the buffer in the due heap */
    public void addDueCardsFromBuffer() {
	//The following might check for <=, but anything less than the current 
	//turn should have been added already
	if(buffer1.getDueTurn() == currentTurn) {
	    dueCards.addAll(buffer1);
	    buffer1 = buffer1.next();
	}
    }

    /* Adds all the cards in the active heap with the current turn number to the 
       due heap and sinks those cards. */
    public void addDueCardsFromHeap() {
	Card c = actives.peek();//peek() returns (but does not remove) the root of the heap
	if(c.getDueTurn() == currentTurn) {
	    dueCards.add(c);
	    actives.remove(c);
	    addDueCardsFromHeap();//this (recursive) loop terminates because of the previous line
	}
    }
    
    /*Takes the current turn number and, if at any point in the buffer there 
      are more cards than turns to ask them on, returns and removes from its 
      BufferObject the card with minimum frequency of all cards up to that 
      point. Adds BufferObjects at the end of the buffer until bound is reached
      if the previous condition is not met before the end of the buffer. */
    public Card lookahead() {
	int totNumber = 0;
	BufferObject curMin = buffer1;
	for(BufferObject bo=buffer1; bo != null; bo=bo.next()) {
	    totNumber = totNumber + bo.size();
	    if(bo.getMin().getFrequency() < curMin.getMin().getFrequency()) {
		curMin = bo;
	    }
	    if(bo.isLast()) {
		bo.addActiveCards(actives, totNumber);
	    }
	    if(totNumber >= bo.getDueTurn() - currentTurn) {
		return curMin.removeFirst();
	    }
	}
	return null;//remember the other return value in the >= clause
    }

    /* Takes the current turn number and returns and removes the card with the 
       minimum percent early, as determined by BufferObject.percentEarly(). */
    public Card leastEarly() {
	BufferObject curMin = buffer1;
	int bufferSize = 0;
	for(BufferObject bo = buffer1;bo!=null;bo=bo.next()) {
	    Card c = bo.getMin();
	    if(bo.percentEarly(currentTurn) < curMin.percentEarly(currentTurn)) {
		curMin = bo;
	    }
	    bufferSize++;
	    if(bo.isLast()) {
		bo.addActiveCards(actives, bufferSize);
	    }
	}
	return curMin.removeFirst();
    }

    /* Called after a card has been quizzed. The parameter is passed whether 
       the user clicked that he knew the answer or he did not know the answer.*/
    public void respond(boolean knewAnswer) {
	current.factorFrequency(knewAnswer);
	current.setDueTurn(current.getDueTurn() + current.getPeriod());
	if(current.getDueTurn() <= currentTurn) {//if the card would go into the due heap
	    current.setActive(false);//this is no longer done on the QList because there is no QList
	    passives.add(current);
	} else {
	    //I could store the last buffer object or its turn number to see 
	    //whether the card belonged in the buffer at all, but at the moment
	    //I am not going to make that optimization
	    if(!addToBuffer(current)) {//if the card is not added to the buffer because it doesn't belong there
		actives.add(current);
	    }
	}
    }
    
    /* Tries to add the card parameter to the buffer. Returns true if it is 
       added and false if not. */ 
    private boolean addToBuffer(Card c) {
	for(BufferObject bo = buffer1;bo != null;bo=bo.next()) {
	    if(c.getDueTurn() == bo.getDueTurn()) {
		bo.add(c);
		return true;
	    }
	}
	return false;
    }

    //I may need to make the following two comparators implement Serializable

    /*Returns the comparator, inconsistent with equals, that says that the 
      cards in the due heap should be sorted so that the card with the greatest
      frequency is at the root */
    public Comparator<Card> dueHeapSorter() {
	Comparator<Card> sorter = new Comparator<Card>() {
	    public int compare(Card o1, Card o2) {
		double d = o1.getFrequency() - o2.getFrequency();
		return (int) Math.signum(d);
	    }
	};
	return sorter;
    }

    /* Returns the comparator, inconsistent with equals, that says that the 
       cards is the active heap should be sorted so that the card with the 
       least turn number is at the root. */
    public Comparator<Card> activeHeapSorter() {
	Comparator<Card> sorter = new Comparator<Card>() {
	    public int compare(Card o1, Card o2) {
		return o1.getDueTurn() - o2.getDueTurn();
	    }
	};
	return sorter;
    }
    
    //Probably need to move saving code from Display to here
}
