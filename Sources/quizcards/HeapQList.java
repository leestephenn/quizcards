/*A subclass of QList that maintains its active cards in a 0-based binary heap.  The heap is a min-heap for its 
  card's due values, so an element's parent with always have a lower due value than it and the card at zero will be the
  one in the heap with the lowest due value*/
package quizcards;
public class HeapQList extends QList { 
    //so the deserializer knows this is the same class if I change it a little
    static final long serialVersionUID = -8019354484074106269L;
    
    public HeapQList() {//make an empty HeapQList with an empty list
    }

    /*make a HeapQList from the QList parameter q by structuring the QList into a heap
      and changing all the due values to turn numbers */
    public HeapQList(QList q) {
	//add each card in the QList parameter to this heap, but do not turn them into a heap
	getList().ensureCapacity(q.size());//ensure that this HeapQList's list has the same size as the QList
	for(int i=0;i<q.size();i++) {
	    Card clone = (Card) q.getCard(i).clone();//clone the card so that its due value in the original QList doesn't change
	    super.addCard(clone);
	}
	//invert the due values so that the card with the largest due value now has the smallest one, etc.
	for(int i=0;i<=lastActiveIndex();i++) {
	    Card c = getCard(i);
	    int newDue = (int) ( (1-c.getDue())/c.getFrequency());
	    c.setDueTurn(newDue);
	}
	//Now turn everything into a heap
	for(int i = parentIndex(lastActiveIndex()); i >= 0; i--) {
	    sink(i);//sinking each element structures the QList into a heap.
	}
	sourceFile = q.getSourceFile();
    }

    /* make a HeapQList from the ArrayList parameter by adding each card in the ArrayList sequentially */
    public HeapQList(java.util.ArrayList<Card> cards) {
	super(cards);//adds each card sequentially
    }


    /* swap the cards at positions i and j in the Arraylist */
    private void swap(int i, int j) {
	Card temp=getCard(i);
	java.util.ArrayList<Card> list = getList();
	list.set(i, getCard(j));
	list.set(j, temp);
    }

    /* Returns the index of one of the children of of the card at parameter i, Call with child equal to zero for the left
       child or with child equal to 1 for the right child.*/
    private int childIndex(int i, int child) {
	return 2*i + 1 + child;
    }

    /* Returs the index of the parent of the card at paremeter i.
       The index of the parent of the card at 0, the root card, is 0. */
    private int parentIndex(int i) {
	return (i-1)/2;//integer division rounds down in Java so the parent of 0 is 0 because -1/2 == 0
    }

    /* The comparator for this heap.  In other words, less(i, parentIndex(i)) should always be false. 
       It compares the due values of the cards at its parameter indices.  */
    public boolean less(int i, int j) {
	Card c=getCard(i);
	Card d=getCard(j);
	return c.getDueTurn() < d.getDueTurn();
    }

    /* The method restores the heap property when one knows that the card at i may have a lower (but not higher) 
       due value than its parent. */
    private void swim(int i) {
	int parentI=parentIndex(i);
	while(less(i, parentI)) {//when the card is less than its parent.  If the current index, i, is zero, then its parent is also zero and thus equal to i, and obviously !less(i, i)
	    swap(parentI, i);
	    i=parentI;
	    parentI=parentIndex(i);
	}
    }

    /* Restores the heap property when one knows that the card at i may have a higher (but not lower) due value 
       than its children. */
    private void sink(int i) {
	while(childIndex(i, 0) <= lastActiveIndex()) {
	    int childI=childIndex(i, 0);
	    if(childI < lastActiveIndex()) {
		if(less(childIndex(i, 1), childI)) {
		    childI=childIndex(i, 1);
		}
	    }
	    if(less(i, childI)) {//if the parent Card is less than the lesser of its two children
	        return;
	    }
	    swap(i, childI);
	    i = childI;
	}
    }

    /* Add the parameter to HeapQList */
    public void addCard(Card c) {
	super.addCard(c);
	if(c.getActive()) {
	    swim(lastActiveIndex());//the card was added at the bottom of the heap at the last active index, so swim that index
	}
    }

    //changes the due value of the minimum card and moves it to its proper place after it has been looked at
    public void changeMin() {
	Card oldMin=getCard(0);//the minimum card
	oldMin.setDueTurn(oldMin.getDueTurn() + oldMin.getPeriod());
	sink(0);
    }

    /* Set the card at parameter i active */
    public void setActive(int i) {
	super.setActive(i);
	Card c = getCard(lastActiveIndex());//get the card that was just set active
	c.setDueTurn(getCard(0).getDueTurn() + 1);//set its due value to that of the root of the heap - change test code to test for this instead
	swim(lastActiveIndex());
    }

    /* Set the card at the parameter i passive */
    public void setPassive(int i) {
	super.setPassive(i);//this swaps the card at i with the the last active card
	if(less(i, lastActiveIndex() + 1)) {//if the card that I moved had a less due value than the card it was swapped with
	    swim(i);
	} else {
	    sink(i);
	}
    }
    public java.io.File getSourceFile() {
	if(sourceFile == null) {
	    return null;
	}
	String name = sourceFile.getPath();
	int dotIndex = name.lastIndexOf(".");
	String newName = name.substring(0, dotIndex + 1) + "hql";
	return new java.io.File(newName);
    }
}
