package quizcards;
public class Proctor {
    private HeapQList list;//the list which holds this proctor's cards
    private Card current;//the card we are currently testing on
    private double sumFreq;//the sum of the frequencies of the cards in the current QList

    /* Make a new proctor from the QList (or HeapQList)*/
    public Proctor(QList q) {//constructor that sets the list of this proctor to a QList
        setQList(q);
    }
    public Proctor(HeapQList hq) {
	setQList(hq);
    }

    /* Sets the list of this Proctor to the HeapQList parameter */
    public void setQList(HeapQList hqlist) {
        if(hqlist==null) {//the the list in the argument is not null
             throw new NullPointerException("A null Qlist was passed to the Proctor");
        }
	//because setQList(QList) calls this method, the following lines are called for all QLists passed in to Proctor
	//the following assumes that if the first card has been converted, they all have
	//The first condition checks the list has a first card
	if(hqlist.size() > 0 && !(hqlist.getCard(0) instanceof ExponentialCard)) {
	    hqlist.makeExponential();
	}
        list=hqlist;//set this proctor's list to the argument
        calcSumFreq();//calculate the sum of the frequencies of the cards in the list
    }

    /* Sets the list of this Proctor to a new HeapQList of the QList parameter */
    public void setQList(QList qlist) {//called when the QList is not a HeapQList
	HeapQList hq = new HeapQList(qlist);//make a HeapQList out of the QList
	setQList(hq);//set the list to that HeapQList
    }

    /* Returns the nextCard to be quizzed upon, the minimum card in the list,
       and sets the current-card variable to that card */
    public Card nextCard() {
        current=list.getCard(0);//set the current-card variable to the minimum card in the heap
	if(!current.getActive()) {//if this is the very first time a card of this list has been tested
	    list.setActive(0);
	}
        return current;//return that card
    }

    /* sets the sum of frequencies equal to the actual sum of the frequencies 
       of the the cards in the list */
    private void calcSumFreq() {
        sumFreq=0.0;//set the sum of frequencies to zero
        for(int i=0;i<=list.lastActiveIndex();i++) {//for all active cards in the list
            Card c=list.getCard(i);//get the ith active card
            sumFreq=c.getFrequency() + sumFreq;//set the sum of frequencies to itself plus frequency of the card we just got
        }
    }

    /*gets this proctor's QList*/
    public QList getQList() {
        return list;
    }

    /*gets the sum of all the card's frequencies*/
    public double getSumFrequency() {
        return sumFreq;
    }
    
    /*Returns whether all the cards in the list are active*/
    public boolean allActive() {
	return (list.lastActiveIndex() == list.size() - 1);
    }

    /* Called when the user says he knew the answer to the current card. 
       Factors the current card's frequency by the correct factor*/
    public void knewResponse() {
	current.factorFrequency(true);
	calcSumFreq();//recalculate the sum of frequencies
	//then introduce a new card with due value calculated from the current card's unchanged due value
	introduceCard();//introduce a card if it should be introduced
	//then change its due value
	list.changeMin();//change the due value of the card and re-arrange the heap to account for it
    }//end method

    /*This method makes the first passive card in the list active if 
      the sum of frequencies plus half the first passive card's frequency is less than 1.
      Called by knewResponse() */
    public void introduceCard() {
	int firstP=list.lastActiveIndex() + 1;//make an int that is the index of the fist passive card
	if(firstP < (list.size())) {//if the index of the first passive card is less than the list's size
	    Card l=list.getCard(firstP);//get the first passive card
	    if((sumFreq + (l.getFrequency()/2))<1) {//if the sum of frequencies will be closer to 1 if the frequency of this card is added to it
		list.setActive(firstP);//set the last passive card active
		calcSumFreq();
	    }
	}
    }

    // Called when the user says he did not know the answer to the current card
    public void notKnewResponse() {
	current.factorFrequency(false);
	calcSumFreq();
	setAsideCard();//sets aside a card if it should be
	list.changeMin();
    }//end method
    
    /* Sets a card passive if it should be set passive*/
    public void setAsideCard() {
	if(sumFreq > (current.getFrequency()/2) + 1) {
	    //the following should be O(1) because indexOf goes through the list starting at 0
	    //which should be the index of the current card
	    int i=list.indexOf(current);//get the index of the current card
	    list.setPassive(i);//make the current card passive
	    calcSumFreq();//recalculate the sum of frequencies
	}
    }
}
