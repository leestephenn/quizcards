package quizcards;//this classes package
public class Card implements java.io.Serializable, Cloneable {
    private String q;//the question this card asks    
    private String a;//the answer to that question
    private boolean activeType=false;//whether this question is active or not
    private double freq = 1.0/6;//the frequency this card should be asked
    private double due=0;//keeps track of how soon this card should be asked
    private int dueTurn;//the turn at which this card should next be asked.  Basically deprecates the due value
    private double corFact=.75;//the ratio that the frequency will be diminished when this question is answered correctly
    private double incorFact=1.0/3;//the ratio that frequency will be diminished when this question is answered correctly
    static final long serialVersionUID = -5757848398331077126L;//paste in the old serial version id
        
    public Card(String question, String answer) {//main Constructer
        setQuestion(question);//set the question via a setter method
        setAnswer(answer);//set the answer via a setter method
    }
    public Card(String question, String answer, boolean active) {//constructor that also takes a boolean
        this(question, answer);//call the other, main constructor to set the question and answer
        setActive(active);//set the active variable
    }
    public Card(String question, String answer, boolean active, double frequency, double cFactor, double iFactor) {//constructor that sets EVERYTHING
        this(question, answer);//set the question and answer using that constructor
        setActive(active);//set the active variable
        setFrequency(frequency);//set this card's frequency
        setCorrectFactor(cFactor);//set the factor the frequency is diminished by when this card is answered correctly
        setIncorrectFactor(iFactor);//set the factor the frequency is increased by when this card is answered INcorrectly
    }
    
    public void setQuestion(String question) {//sets the card's question to the parameter
        if(validateQuestion(question)) {//if the question is valid
            q=question;//set the question instance variable to the question parameter
        } else {
            System.out.println("value was empty");//the answer is not set and a message is printed out
        }//close if-else
    }
    public static boolean validateQuestion(String question) {//validates the question
        if(question==null) {//if the parameter is null
            throw new NullPointerException("Question was null");//throw an appropriate exception
        }
	boolean r=true;//defaule the question wasn't empty
        if("".equals(question)) {//if the question was empty
	    r=false;//set the return value tofalse
	}
	return r;
    }
    public void setAnswer(String answer) {//sets the card's answer to the parameter
        if(validateAnswer(answer)) {//if the answer is valid
            a=answer;//set the question intstance variable to the question parameter
        } else {
            System.out.println("value was empty");//the answer is not set and a message is printed out
        }//end if-else
    }
    public static boolean validateAnswer(String answer) {
        if(answer==null) {//if the answer is null
            throw new NullPointerException("Answer was null");//throw an appriopriate exception
        }
	boolean r=true;//defaule the question wasn't empty
        if("".equals(answer)) {//if the question was empty
	    r=false;//set the return value tofalse
	}
	return r;
    }
    public void setActive(boolean activity) {//sets whether the card is active or not (not in constructor because default false
        activeType=activity;//set the instance variable about whether the card is active to the parameter
    }
    public void setFrequency(double frequenc) {//sets the frequency this card is asked
        validateFrequency(frequenc);//validate the frequency
        freq=frequenc;//set the frequency variable to the frequency parameter
    }
    public static void validateFrequency(double frequenc) {//validates the frequency
        if((frequenc>1.)||(frequenc<=0.)) {//if the frequency is not between 0 and 1
            throw new IllegalArgumentException("Frequency was " + frequenc + ", not between 0 and 1");//throw an exception
        }
    }
    /* Sets the frequency to the reciprocal of the parameter */
    public void setPeriod(int period) {
	setFrequency(1.0/period);
    }
    /*Now that the due value is not incremented from zero to 1 each turn, one should probably use dueTurn */
    public void setDue(double d) {//sets the variable that tells us how soon this card is due for display
	validateDue(d);//validate the due value (an exception will be thrown if the parameter isn't valid
	due=d;//set the due variable to the parameter.
    }
    public static void validateDue(double d) {//validates the due value
        /* Changed for HeapQList 
	   if(d<0.) {//if the parameter is negative
	    throw new IllegalArgumentException("The due value, " + d + ", was negative");//throw an exception
	}
	*/
    }
    /*sets the dueTurn variable to the parameter */
    public void setDueTurn(int d) {
	dueTurn = d;
    }
    /*returns the dueTurn variable */
    public int getDueTurn() {
	return dueTurn;
    }
    public void setCorrectFactor(double cFactor) {//sets the factor the frequency is diminished by when this card is answered correctly
        validateCorrectFactor(cFactor);//validate the parameter (an exception will be thrown if the parameter isn't valid
	corFact=cFactor;//set the variable for this factor to the parameter for this factor
    }
    public static void validateCorrectFactor(double cFactor) {//validates the correct factor
        if(!((cFactor<1)&&(cFactor>0))) {//if the parameter is not between (excluding both) 0 and 1
	    throw new IllegalArgumentException("Correct factor was " + cFactor + ", not between 0 and 1");//throw an exception
	}
    }
    public void setIncorrectFactor(double iFactor) {//sets the factor the frequency is increased by when this card is answered incorrectly
        validateIncorrectFactor(iFactor);//validate the parameter (an exception will be thrown if the parameter isn't valid)
	incorFact=iFactor;//set the incorrect factor variable to the incorrect factor parameter
    }
    public static void validateIncorrectFactor(double iFactor) {//validates the incorrect factor
        if(!(iFactor>0)) {//if the parameter is not greater than 0
	    throw new IllegalArgumentException("Incorrect factor was " + + iFactor + ", not greater than 1");//throw an exception
	}
    }    
    public String getQuestion() {//gets the question
        return q;//return the question
    }
    public String getAnswer() {//gets the answer
        return a;//returns the answer
    }
    public boolean getActive() {//get whether this card is active
        return activeType;// return whether this card is active
    }
    public double getFrequency() {//gets the frequency this card is asked
        return freq;//return the frequency variable
    }
    /* A convenience method that returns the reciprocal of the frequency rounded to the nearest int */
    public int getPeriod() {
	return (int) Math.round(1.0/freq);//Math.round returns a long
    }
    public double getDue() {//gets the due value
        return due;//return the due value
    }
    public double getCorrectFactor() {//gets the factor the frequency is diminished by when this card is answered correctly
        return corFact;//return the correct-factor variable
    }
    public double getIncorrectFactor() {//gets the factor the frequency is increased by when this card is answered incorrectly
        return incorFact;//return the incorrect-factor variable
    }
    public String toString() {//what will print when System.out.println(new Card()) is called
        return q + " " + a;//return a combination of the question and answer
    }
    /*Overrides Object.clone().  Returns a new Card that is the same except that it is not == to this one. */
    public Object clone() {
	Card clone = null;
	try {
	    clone = (Card) super.clone();//this does almost all the work by making a copy of all the instance variables
	    clone.q = new String(clone.q);
	    clone.a = new String(clone.a);
	} catch (CloneNotSupportedException e) {}//this should not happen because we implement Cloneable
	return clone;
    }
    
    /*Returns this card as an exponential card. 

      I dislike its presence here because it makes Card and ExponentialCard mutually dependent.
      This is put here so it has access to the instance variables.
      It wouldn't have access to them to SET in ExponentialCard because the variables are in this class. */
    public ExponentialCard asExponentialCard() {
	ExponentialCard ec = new ExponentialCard(q, a, activeType);
	ec.setFrequency(freq);
	ec.setDue(due);
	return ec;
    }
    /* Factors the frequency by the correct factor or incorrect factor depending 
       on whether the parameter is true or false, respectively. */
    public void factorFrequency(boolean knew) {
	double factor = knew ? corFact : incorFact;
	try {//the following can throw (unchecked) exceptions
	    setFrequency(factor*freq);
	} catch (IllegalArgumentException ex) { }//in which case we simply do nothing
    }
}
