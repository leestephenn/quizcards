package quizcards;
import java.text.DecimalFormat;
/*Card that implements the new, exponential frequency change algorithm */
public class ExponentialCard extends Card {
    static final long serialVersionUID = 8221066924764232685L;//puts responsibility for making changes work on me
    static double upperBound = Math.log(3);//the upper bound of the frequency change exponent
    static double lowerBound = -Math.log(2);//the lower bound of the frequency change exponent
    static double gFactor = 4.0 / 3.0;//the growth factor for when the user is answering a card correctly
    static double hFactor = 3.0 / 2;//the growth factor for when the user is answering a card incorrectly
    static double convergeFactor = 0.5;//how fast the frequency change exponent will converge to 0
    
    private double exponent = .5;//the variable which the period is factored by the exponential of

    /*Constructors are not inherited, so the following two must be explicitly written.
      They do nothing more than call the super class constructor with the same parameters, however. */

    public ExponentialCard(String question, String answer) {
	super(question, answer);
    }
    
    public ExponentialCard(String question, String answer, boolean active) {
	super(question, answer, active);
    }
    /*this sets everything, but does not set correctFactor and incorrectFactor because those are deprecated*/
    public ExponentialCard(String question, String answer, boolean active, double frequency, double exp) {
	this(question, answer, active);//call our constructor for the first three variables
	setFrequency(frequency);
	setExponent(exp);
    }

    /*returns the factor which makes the lower bound a fixed point */
    static double rFactor() {
	return (1 - gFactor) / lowerBound;
    }
    /*returns the factor which makes the upper bound a fixed point. */
    static double sFactor() {
	return (hFactor - 1) / upperBound;
    }
    

    /*Sets the factor exponent to the parameter.  Throws an exception if it is not between 
      upperBound and lowerBound */
    public void setExponent(double exp) {
	validateExponent(exp);
	exponent = exp;
    }
    public static void validateExponent(double exp) {
	if(exp > upperBound || exp < lowerBound) {
	    DecimalFormat df = new DecimalFormat("##0.000");
	    String tUpperBound = df.format(upperBound);
	    String tLowerBound = df.format(lowerBound);
	    throw new IllegalArgumentException("The exponent parameter, " + exp + ", was not between the upper bound, "
		    + tUpperBound + ", and the lower bound, " + tLowerBound + ".");
	}
    }

    /* Returns the factor exponent. */
    public double getExponent() {
	return exponent;
    }

    /* changes the factor exponent according to whether it is positive or negative and to the parameter, whether 
       the user knew the answer to the question or not, and factors the frequency by e to the factor exponent */
    public void factorFrequency(boolean knew) {
	if(knew ^ exponent > 0) {
	    exponent = - exponent * convergeFactor;
	} else {//both booleans are the same, so we only have to test one. Also, exponent != 0
	    if(knew) {
		exponent = exponent * (hFactor - sFactor() * exponent);
	    } else {
		exponent = exponent * (gFactor + rFactor() * exponent);
	    }
	}
	double newFreq = Math.exp(- exponent) * getFrequency();
	newFreq = newFreq > 1.0 ? 1.0 : newFreq;//setFrequency throws an excepition if its argument is > 1
	setFrequency(newFreq);
    }
    /* Overrides this method in Card, which is used there to convert instances of that class to instances of this class.
       Here, this just returns this object. */
    public ExponentialCard asExponentialCard() {
	return this;
    }
    
    /* The correctFactor and incorrectFactor variables are effectively deprecated by the new exponent variable.
       Thus, the following two methods are deprecated, and simply return e^-exponent as the best functional replacement.
       This is probably NOT the behaviour you want, so use factorFrequency() instead. 
       The setter methods do nothing.  Their functionality is largely replaced by setExponent() */

    /* DEPRECATED -- see above */
    public double getCorrectFactor() {
	return Math.exp(-exponent);
    }

    /* DEPRECATED -- see above */
    public double getIncorrectFactor() {
	return getCorrectFactor();
    }

    /* DEPRECATED -- see above */
    public void setCorrectFactor(double corFact) {
    }

    /* DEPRECATED -- see above */
    public void setIncorrectFactor(double incorFact) {
    }
}
