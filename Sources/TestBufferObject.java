import quizcards.*;
import java.util.*;
public class TestBufferObject {
    public static void main(String[] args) {
	System.out.println("Test of add() method");
	testAdd();
	System.out.println("First test of add() method");
	testAdd1();
	System.out.println("Second test of add() method");
	testAdd2();
	System.out.println("Test of addAll() method");
	testAddAll();
	System.out.println("Test of new BufferObject(collection) method");
	testNewOfCollection();
	System.out.println("Test of the setNext() method");
	testSetNext();
	System.out.println("Test of the next() method");
	testNext();
	System.out.println("Test of setBound() method");
	testSetBound();
	//testForLoop();
    }
    
    public static void testAdd() {
	BufferObject bo = new BufferObject();
	for(int i = 0;i < 10;i++) {
	    int oldSize = bo.size();
	    Card c = indexedCard(i); 
	    bo.add(c);
	    if(bo.size() != oldSize + 1) {
		System.err.println("The buffer's size was not incremented when a card was added to it.");
	    }
	    if(!bo.contains(c)) {
		System.err.println("The BufferObject did not contain the card, " + c + " that was just added to it");
	    }
	}
    }

    public static void testAdd1() {
	BufferObject bo = new BufferObject();
	for(int i = 1;i<6;i++) {
	    Card c = indexedCard(i);
	    c.setFrequency(1.0/i);
	    bo.add(c);
	}
	verifySorted(bo);
    }

    public static void testAdd2() {
	BufferObject bo = new BufferObject();
	Card first = new Card("When will it be due?", "Now!");
	first.setDueTurn(3);
	bo.add(first);
	Card diff = new Card("Why am I so lonely?", "Because you're different.");
	diff.setDueTurn(5);
	try {
	    bo.add(diff);
	    System.err.println("An exception was not thrown when a card with a different due value was added to the " + 
		    "buffer.");
	} catch(IllegalArgumentException ex) {
	}
    }

    public static void testAddAll() {
	ArrayList<Card> vec = cardCollection();
	BufferObject bo = new BufferObject();
	int oldSize = bo.size();
	bo.addAll(vec);
	//the following in effect verifies that add() was called
	if(bo.size() != oldSize + vec.size()) {
	    System.err.println("The size of the buffer object was not incremented by the number of cards in the" + 
		    "collection added.");
	}
	verifySorted(bo);
    }

    public static void testNewOfCollection() {
	ArrayList<Card> vec = cardCollection();
	BufferObject bo = new BufferObject(vec);
	verifySorted(bo);
    }

    public static void verifySorted(BufferObject bo) {
	Iterator<Card> it = bo.iterator();
	Card lastCard = it.next();
	for(Card c = it.next();it.hasNext();c = it.next()) {
	    if(!(lastCard.getFrequency() < c.getFrequency())) {
		System.err.println("The cards " + lastCard + " and " + c + " were not sorted by increasing frequency.");
	    }
	    lastCard = c;
	}
    }

    public static Card indexedCard(int i) {
	return new Card("What is 1 + " + i + "?", (1 + i) + ".");
    }

    public static ArrayList<Card> cardCollection() {
	ArrayList<Card> vec = new ArrayList<Card>(5);
	for(int i= 1;i<6;i++) {
	    Card c = indexedCard(i);
	    c.setFrequency(1.0/i);
	    vec.add(c);
	}
	return vec;
    }

    public static void testSetNext() {
	BufferObject first = new BufferObject();
	try {
	    first.setNext(new BufferObject());
	    System.err.println("An exception was not thrown when next was set to an empty BufferObject.");
	} catch(IllegalArgumentException ex) {
	}
    }

    public static void testNext() {
	BufferObject prev = new BufferObject();
	BufferObject first = prev;
	prev.add(indexedCard(0));
	for(int i=1;i<3;i++) {
	    BufferObject next = new BufferObject();
	    next.add(indexedCard(i));
	    prev.setNext(next);
	    prev = next;
	}
	BufferObject second = first.next();
	second.remove(0);
	if(first.next() == second) {
	    System.err.println(second + ", an empty BufferObject, was returned by next()");
	}
    }

    public static void testForLoop() {
	BufferObject prev = new BufferObject();
	BufferObject first = prev;
	prev.add(indexedCard(0));
	for(int i =1;i<10;i++) {
	    BufferObject next = new BufferObject();
	    next.add(indexedCard(i));
	    prev.setNext(next);
	    prev = next;
	}
	for(BufferObject bo = first;bo != null; bo = bo.next()) {
	    System.out.println(bo);
	}
    }

    public static void testSetBound() {
	try {
	    BufferObject.setBound(-1);
	    System.err.println("The bound was set to a negative number.");
	} catch (IllegalArgumentException ex) {
	}
    }

}
