import java.io.File;
import quizcards.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Random;
/*Runs through a QList without Display */
public class DryRunProctor {
    private Proctor p;
    private LinkedHashMap<Card, LinkedList<CardChange>> table = new LinkedHashMap<Card, LinkedList<CardChange>>();

    public static void main(String[] args) {
	if(args.length == 0) {
	    (new DryRunProctor(testingQList())).go();
	} else {
	    QList q = QList.readQList(new File(args[0]));
	    (new DryRunProctor(q)).go();
	}
    }

    public DryRunProctor(QList q) {
	p = new Proctor(q);
	QList expList = p.getQList();
	for(int i= 0;i<expList.size();i++) {
	    table.put(expList.getCard(i), new LinkedList<CardChange>());
	}
    }

    public void go() {
	Random random = new Random();
	for(int i=0;i<50;i++) {
	    ExponentialCard c = (ExponentialCard) p.nextCard();
	    CardChange cc = new CardChange(i, c.getFrequency(), c.getExponent());
	    boolean known = random.nextDouble() < 2.0/3;
	    if(known) {
		p.knewResponse();
	    } else {
		p.notKnewResponse();
	    }
	    cc.setNewValues(known, c.getFrequency(), c.getExponent());
	    System.out.println("SumFreq: " + p.getSumFrequency() + " " + c + " " + cc);
	    LinkedList<CardChange> changes = table.get(c);
	    changes.addLast(cc);
	}
	/*for(Card key : table.keySet()) {
	    System.out.print(key + ": ");
	    System.out.println(table.get(key));
	}*/
    }
    public static HeapQList testingQList() {
	HeapQList r = new HeapQList();
	for(int i = 0;i < 12; i++) {
	    r.addCard(new Card("What is 1 + " + i + "?", (1+i) + "."));
	}
	return r;
    }
}
