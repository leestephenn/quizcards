import java.util.Random;
import quizcards.Card;
public class TestCard {
    public static void main(String[] args) {
	System.out.println("Test of clone method");
	testClone();
	System.out.println("Test of getPeriod() and automatic set of frequency.");
	testNewFrequencyStuff();
	System.out.println("Zeroth test of factorFrequency()");
	testFactorFrequency0();
	System.out.println("First test of factorFrequency()");
	testFactorFrequency1();
	System.out.println("Test of asExponentialCard()");
	testAsExponentialCard();
    }
    public static void testClone() {
	for(int i = 0;i<50;i++) {
	    Random r = new Random();
	    String question = "" + r.nextDouble();
	    String answer = "" + r.nextDouble();
	    boolean active = r.nextBoolean();
	    double frequency = r.nextDouble();
	    double correctFactor = r.nextDouble();
	    double incorrectFactor = r.nextDouble();
	    Card c = new Card(question, answer, active, frequency, correctFactor, incorrectFactor);
	    Card d = (Card) c.clone();
	    if(c == d) {
		System.err.println("A card and its clone were ==");
	    }
	    if(c.getQuestion() == d.getQuestion()) {
		System.err.println("A card's question and its clone's question were ==");
	    }
	    if(c.getAnswer() == d.getAnswer()) {
		System.err.println("A card's answer and its clones's answer were ==");
	    }
	    if(!verifyEqual(c, d)) {
		System.err.println("A card was not equal to its clone.");
	    }
	}
    }
    public static boolean verifyEqual(Card c, Card d) {
	boolean r = true;
	r = r && c.getQuestion().equals(d.getQuestion());
	r = r && c.getAnswer().equals(d.getAnswer());
	r = r && (c.getActive() == d.getActive());
	r = r && (c.getFrequency() == d.getFrequency());
	r = r && (c.getCorrectFactor() == d.getCorrectFactor());
	r = r && (c.getIncorrectFactor() == d.getIncorrectFactor());
	return r;
    }
    /* Tests two new frequency features: the getPeriod method, and the automatic set to 1/6 */
    public static void testNewFrequencyStuff() {
	Card c = new Card("First?", "Yes, first");
        if(c.getFrequency() == 0) {
	    System.err.println("The frequency was not automatically set to a value.");
	}
	if(c.getPeriod() != 1.0/c.getFrequency()) {
	    System.err.println("The period was not the inverse of the frequency, " + c.getFrequency() + 
		    ", but was " + c.getPeriod());
	}
    }

    /*Test of the new factorFrequency method*/
    public static void testFactorFrequency0() {
	Card c=new Card("First?", "Yes, first.");
	c.setCorrectFactor(.75);
	double oldFreq = c.getFrequency();
	c.factorFrequency(true);
	if((c.getFrequency() / oldFreq) != c.getCorrectFactor()) {
	    System.err.println("The frequency was " + c.getFrequency() + " not the old frequency times the correct " +
		    "factor when called on true.");
	}
	c = new Card("Second?", "Yes, second");
	c.setIncorrectFactor(1.0/3);
	oldFreq = c.getFrequency();
	c.factorFrequency(false);
	if((c.getFrequency() / oldFreq) != c.getIncorrectFactor()) {
	    System.err.println("The frequency was " + c.getFrequency() + " not the old frequency times the incorrect " +
		    "factor when called on false.");
	}
    }
    /*Test of what the factorFrequency method does when called for an invalid value */
    public static void testFactorFrequency1() {
	Card c = new Card("Mad?", "Yes, mad");
	c.setIncorrectFactor(10.0);
	double oldFreq = c.getFrequency();
	try {
	    c.factorFrequency(false);
	} catch(IllegalArgumentException ex) {
	    System.err.println("The factor frequency method threw an exception when the new frequency was invalid.");
	}
	testVariableEquality("frequency", c.getFrequency(), oldFreq);
    }
    /* Tests the new asExponentialCard method */
    public static void testAsExponentialCard() {
	Card c = new Card("a?", "b.", true, .25, .75, 1.3);
	quizcards.ExponentialCard ec = c.asExponentialCard();
	if(! (ec instanceof quizcards.ExponentialCard)) {
	    System.err.println("The card was not converted to an ExponentialCard.");
	}
	testVariableEquality("question", c.getQuestion(), ec.getQuestion());
	testVariableEquality("answer", c.getAnswer(), ec.getAnswer());
	testVariableEquality("activity", c.getActive(), ec.getActive());//hopefully autoboxing will work
	testVariableEquality("frequency", c.getFrequency(), ec.getFrequency());
	testVariableEquality("due value", c.getDue(), ec.getDue());
	//correct factor and incorrect factor aren't tested since their functionality in the two classes is different
    }
    public static void testVariableEquality(String name, Object oldValue, Object newValue) {
	if(! (oldValue.equals(newValue))) {
	    System.out.println("The ExponentialCard's " + name + " was " + newValue + 
		" not the same as the old value, " + oldValue);
	}
    }
}
