import quizcards.*;
public class TestQList {
    /*runs all the tests */
    public static void main(String[] args) {
	System.out.println("Test of makeExponential()");
	testMakeExponential();
    }
    public static void testMakeExponential() {
	QList oldQ = new QList();
	QList q = new QList();
	for(int i=0;i<30;i++) {
	    double d = Math.random();
	    Card c = null;
	    if(d < 0.5) {
		c = new ExponentialCard("q" + i, "a" + i);
	    } else {
		c= new Card("q" + i, "a" + i);
	    }
	    oldQ.addCard(c);
	    q.addCard(c);
	}
	q.makeExponential();
	for(int i=0;i<q.size();i++) {
	    if(! (q.getCard(i) instanceof ExponentialCard)) {
		String className = oldQ.getCard(i).getClass().getName();
		System.err.println("The " + className + " at " + i + " was not converted to an ExponentialCard.");
	    }
	}
	for(int i=0;i<oldQ.size();i++) {
	    Card c = oldQ.getCard(i);
	    if(c instanceof ExponentialCard) {
		if(c != q.getCard(i)) {
		    System.err.println("The ExponentialCard at " + i + " was converted to a != ExponentialCard.");
		}
	    } else {
		if(c.getFrequency() != q.getCard(i).getFrequency()) {
		    System.err.println("The Card at " + i + " was not properly converted to an ExponentialCard.");
		}
	    }
	}
    }
}
