import quizcards.*;
public class TestExponentialCard {
    /* main method which runs the tests */
    public static void main(String[] args) {
	System.out.println("Test of new ExponentialCard(question, answer)");
	testNewTwoArg();
	System.out.println("Test of new ExponentialCard(question, answer, activity)");
	testNewThreeArg();
	System.out.println("Test of setExponent(double)");
	testSetExponent();
	System.out.println("Test of new ExponentialCard of everything");
	testNewFiveArg();
	System.out.println("Test of asExponentialCard()");
	testAsExponentialCard();
	System.out.println("Test of correct factor and incorrect factor");
	testSetFactor();
	System.out.println("Test of factorFrequency()");
	testFactorFrequency();
	System.out.println("First test of factorFrequency()");
	testFactorFrequency1();
	System.out.println("Second test of factorFrequency()");
	testFactorFrequency2();
	System.out.println("Third test of factorFrequency()");
	testFactorFrequency3();
    }
    
    /*Tests the new ExponentialCard(question, answer) constructor */
    public static void testNewTwoArg() {
	String q = "Once?";
	ExponentialCard ec = new ExponentialCard(q, "Upon.");
	testVariableEquality("question", q, ec.getQuestion());
	//Don't bother testing answer, the constructor should work now that we know it's called
	if(ec.getExponent() == 0.0) {
	    System.err.println("The card, " + ec + ", had an initial exponent of zero");
	}
    }
    /*Test of new ExponentialCard(question, answer, activity) */
    public static void testNewThreeArg() {
	boolean b = true;//false is default
	ExponentialCard ec = new ExponentialCard("A midnight?", "Dreary", b);
	testVariableEquality("activity", b, ec.getActive());
	if(ec.getExponent() == 0.0) {
	    System.err.println("The card, " + ec + ", had an initial exponent of zero");
	}
    }
    /*Test of setExponent(double) */
    public static void testSetExponent() {
	ExponentialCard ec = new ExponentialCard("As I pondered?", "Weak");
	double exp = 0.0;
	ec.setExponent(exp);
	testVariableEquality("exponent", exp, ec.getExponent());
	exp = ec.getExponent();
	try {
	    ec.setExponent(1000.0);
	    //if an exception was not thrown:
	    System.err.println("The exponent was changed to, " + ec.getExponent() + ", which should be greater than " +
		    "its upper bound.");
	} catch(IllegalArgumentException ex) {
	}
	try {
	    ec.setExponent(-1000.0);
	    System.err.println("The exponent was changed to, " + ec.getExponent() + ", which should be less than " +
		    "its lower bound, ");
	} catch(IllegalArgumentException ex) {
	}
    }
    /* Test of new ExponentialCard of everything */
    public static void testNewFiveArg() {
	String q = "And weary?";
	double freq = .25;
	double exp = 0.0;
	ExponentialCard ec = new ExponentialCard(q, "Over many a volume.", true, freq, exp);
	testVariableEquality("question", q, ec.getQuestion());
	testVariableEquality("frequency", freq, ec.getFrequency());
	testVariableEquality("exponent", exp, ec.getExponent());
    }
    /*Test the equality of the two object parameters, and print them out, 
      along with the name of the variable, if they are unequal. */
    public static void testVariableEquality(String name, Object oldValue, Object newValue) {
	if(! (oldValue.equals(newValue))) {
	    System.err.println("The ExponentialCard's " + name + " was " + newValue + 
		" not the same as the old value, " + oldValue);
	}
    }
    /*Test the asExponentialCard method */
    public static void testAsExponentialCard() {
	ExponentialCard ec = new ExponentialCard("Should be the same?", "Yes");
	ExponentialCard same = ec.asExponentialCard();
	if(ec != same) {
	    System.err.println("The return of the asExponentialCard() method" +
		   " was not == to the original ExponentialCard.");
	}
    }
    /* Tests the getCorrectFactor and getIncorrectFactor methods */
    public static void testSetFactor() {
	ExponentialCard ec = new ExponentialCard("Of forgotten lore?", "Yes.");
	testCorrectFactor(ec);
	ec.setExponent(0.0);
	testCorrectFactor(ec);
    }
    /* Run all sorts of different tests of the correctFactor methods */
    public static void testCorrectFactor(ExponentialCard ec) {
	double rightCf = Math.exp(-ec.getExponent());
	if(ec.getCorrectFactor() != rightCf) {
	    System.err.println("The correct factor was, " + ec.getCorrectFactor() + ", not equal to " + rightCf);
	}
	if(ec.getIncorrectFactor() != rightCf) {
	    System.err.println("The incorrect factor was, " + ec.getCorrectFactor() + ", not equal to " + rightCf);
	}
	try {
	    ec.setCorrectFactor(100.5);//this would throw an exception if called on a Card
	    if(ec.getCorrectFactor() != rightCf) {
		System.err.println("The correct factor improperly changed from " + rightCf + " to " + ec.getCorrectFactor());
	    }
	} catch(IllegalArgumentException ex) {
	    System.err.println("The setCorrectFactor method threw an illegal argument exception.");
	}
	try {
	    ec.setIncorrectFactor(-33.3);//this would throw an exception if called on a Card
	    if(ec.getIncorrectFactor() != rightCf) {
		System.err.println("The incorrect factor improperly changed from " + rightCf + " to " + ec.getCorrectFactor());
	    }
	} catch(IllegalArgumentException ex) {
	     System.err.println("The setIncorrect factor threw an illegal argument exception");
	}
    }
    /* test that the factorFrequency() method moves in the right direction for all inputs of exponent boolean knew */
    public static void testFactorFrequency() {
	ExponentialCard ec = new ExponentialCard("Known?", "Only to you");
	double exp = 0.5;
	oneFactorFrequencyTest(ec, exp, true);
	if(! (ec.getExponent() > exp)) {
	    System.err.println("The exponent was " + ec.getExponent() + ", not greater than its old value, " + exp);
	}
	exp = 0.5;
	oneFactorFrequencyTest(ec, exp, false);
	if(! (ec.getExponent() < 0.0)) {
	    System.err.println("The exponent was " + ec.getExponent() + ", not less than 0.");
	}
	if(! (ec.getExponent() > -exp)) {
	    System.err.println("The exponent was " + ec.getExponent() + ", not greater than " + (-exp) + 
		    ", the negative of the old exponent.");
	}
	exp= -0.5;
	oneFactorFrequencyTest(ec, exp, false);
	if(! (ec.getExponent() < exp)) {
	    System.err.println("The exponent was " + ec.getExponent() + ", not less than " + exp);
	}
	exp = -0.5;
	oneFactorFrequencyTest(ec, exp, true);
	if(! (ec.getExponent() > 0.0)) {
	    System.err.println("The exponent was " + ec.getExponent() + ", not greater than 0.");
	}
	if(! (ec.getExponent() < -exp)) {
	    System.err.println("The exponent was " + ec.getExponent() + ", not less than " + (-exp) +
		", the negative of the old exponent.");
	}
    }
    public static void oneFactorFrequencyTest(ExponentialCard ec, double exp, boolean knew) {
	ec.setExponent(exp);
	double oldFreq = ec.getFrequency();
	ec.factorFrequency(knew);
	if(ec.getFrequency() == oldFreq) {
	    System.err.println("The frequency was not changed from " + ec.getFrequency());
	}
    }
    /* Tests factorFrequency's convergence to upperBound when called repeatedly on true and a positive exponent */
    public static void testFactorFrequency1() {
	testFactorFrequencyBounds(true, 0.025, false);
    }
    /* Tests factorFrequency's convergence to lowerBound when called repeatedly on false and a negative exponent. */
    public static void testFactorFrequency2() {
	testFactorFrequencyBounds(false, -0.25, false);
    }
    /* Tests factorFrequency's convergence to zero when called on alternating true and false. */
    public static void testFactorFrequency3() {
	testFactorFrequencyBounds(true, 1.0, true);
    }
    /* Logs the exponent when factorFrequency is called repeatedly on the boolean parameter 
       with an initial exponent of the double parameter. */
    public static void testFactorFrequencyBounds(boolean knew, double exp, boolean invert) {
	//no check that it's going the right way, but oh well
	ExponentialCard ec = new ExponentialCard("Out?", "And away!");
	StringBuilder sb = new StringBuilder(2500);
	ec.setExponent(exp);
	for(int c=0;c<100; c++) {
	    exp = ec.getExponent();
	    sb.append(exp + " ");
	    try {
		ec.factorFrequency(knew);//testing for true each time makes the exponent increase
	    } catch(IllegalArgumentException ex) {
		System.err.println("Error: c: " + c + ", exponent: " + exp + ", factor: " + Math.exp(-exp) + 
			", frequency: " + ec.getFrequency());
		System.err.println(sb);
		throw ex;
	    }
	    knew = knew ^ invert;//switches knew to its negation if invert, does nothing if !invert
	}
	sb.append(ec.getExponent());
	System.err.println(sb);
    }
    public static boolean verifyEqual(Card c, Card d) {
	boolean r = true;
	r = r && c.getQuestion().equals(d.getQuestion());
	r = r && c.getAnswer().equals(d.getAnswer());
	r = r && (c.getActive() == d.getActive());
	r = r && (c.getFrequency() == d.getFrequency());
	return r;
    }
}
