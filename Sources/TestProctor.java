import quizcards.*;
import java.util.ArrayList;
public class TestProctor {
    public static void main(String[] args) {
	System.out.println("Test of setQList(HeapQList hq)");
	testSetQListOfHeapQList();
	System.out.println("Test of setQList(QList q)");
	testSetQListOfQList();
	System.out.println("Test of nextCard()");
	testNextCard();
	System.out.println("Test of introduceCard()");
	testIntroduceCard();
	System.out.println("Zeroth test of knewResponse()");
	testKnewResponse0();
	System.out.println("First test of knewResponse()");
	testKnewResponse1();
    }

    public static double sumOfFreqsForTesting(Proctor p) {
	double sum = 0.0;
	QList q = p.getQList();
	for(int i=0;i<=q.lastActiveIndex();i++) {
	    sum = sum + q.getCard(i).getFrequency();
	}
	return sum;
    }
    /* returns an ArrayList with the cards in backwards due value order */
    public static ArrayList<Card> cardList() {
	ArrayList<Card> arrayL = new ArrayList<Card>();
	for(int i=11;i>=0;i--) {
	    Card c = new Card("Question" + i, "Answer" + i, i < 6);
	    c.setDue(i);
	    c.setFrequency(.0625);
	    c.setCorrectFactor(.75);
	    arrayL.add(c);
	}
	return arrayL;
    }
    /* Makes a list of cards.  Every property except question and answer is left at its default. */
    public static ArrayList<Card> additionList() {
	ArrayList<Card> cards = new ArrayList<Card>();
	for(int i = 0;i < 12; i++) {
	    cards.add(new Card("What is 1 + " + i + "?", (1+i) + "."));
	    //Here I don't do anything like set the due value like a due above
	}
	return cards;
    }
    public static void testSetQListOfHeapQList() {
	HeapQList hq = new HeapQList(cardList());
	Proctor p = new Proctor(new HeapQList());
	p.setQList(hq);
	if(p.getQList()!=hq) {
	    System.err.println("The proctor's list was not set to the HeapQList passed to it.");
	}
	for(int i=0;i<p.getQList().size();i++) {
	    if(! (p.getQList().getCard(i) instanceof ExponentialCard)) {
		System.err.println("The card at " + i + " was not an ExponentialCard.");
	    }
	}
	if(p.getSumFrequency() != sumOfFreqsForTesting(p)) {
	    System.err.println("The proctor did not update the sum of frequencies when passed a new list");
	}
	try {
	    p.setQList(null);
	    //the following line will only run if the preceding line did not throw an exception
	    System.err.println("The proctor did not throw a null pointer exception when passed a null HeapQList");
	} catch(NullPointerException e) { }
    }
    public static void testSetQListOfQList() {
	QList q = new QList(cardList());
	Proctor p = new Proctor(new HeapQList());
	p.setQList(q);
	if(!(p.getQList() instanceof HeapQList)) {
	    System.err.println("The QList was not converted to a HeapQList when the proctor's list was set to it.");
	}
	verifyQListsEqual(q, p.getQList());
    }
    /*tests if the two QLists are equal.  Should override equals() in QList for this, but that didn't work.*/
    private static void verifyQListsEqual(QList q, QList r) {
	if(q.size() != r.size()) {
	    System.err.println("The two QLists' sizes, " + q.size() + " and " + r.size() + ", were not equal.");
	    return;
	}
	boolean ret = true;
	for(int i = 0; i < q.size();i++) {
	    boolean b = TestExponentialCard.verifyEqual(q.getCard(i), r.getCard(i));
	    if(!b) {
		System.err.println("The cards, " + q.getCard(i) + " and " + r.getCard(i) + ", at index " + i + " were not equal.");
	    }
	    ret = b && ret;
	}
	System.out.println(ret);
	if(!ret) {
	    System.err.println("The two QLists were not equal.");
	}
    }
    public static void testNextCard() {
	Proctor p = new Proctor(new HeapQList(cardList()));
	Card c = p.nextCard();
	//NOTE: the test of whether first card in heap and whether current was set must be in this order 
	//because knewResponse changes the heap
	if(c != p.getQList().getCard(0)) {
	    System.err.println("nextCard() did not return the minimum card in the heap.");
	}
	try {
	    p.knewResponse();
	} catch (NullPointerException ex) {
	    System.out.println("Current was not set in nextCard");
	}
    }
    public static void testIntroduceCard() {
	Proctor p=new Proctor(new HeapQList(cardList()));
	HeapQList hq = (HeapQList) p.getQList();
	Card c = hq.getCard(hq.lastActiveIndex() + 1);
	double oldDue = c.getDue();
	p.introduceCard();
	if(c.getDue() == oldDue) {
	    System.err.println("The card's due value was not changed from " + c.getDue() + ".");
	}
    }
    public static void testKnewResponse0() {
	Proctor p = new Proctor(new HeapQList(cardList()));
	Card c = p.nextCard();
	double oldFreq = c.getFrequency();
	double oldDue = c.getDue();
	double oldSumFreq = p.getSumFrequency();
	p.knewResponse();
	//There are actually times when the frequency will not change because 
	//the change is so small and the computer's precision is limitted.
	//I assume I am not in one of those cases.
	if(c.getFrequency() == oldFreq) {
	    System.err.println("The new frequency was " + c.getFrequency() + ", which is == the old frequency");
	}
	if(c.getDue() <= oldDue) {//this serves as a flag that list.changeMin() was called
	    System.err.println("The new due value was " + c.getDue() + ", which is <= the old due value, " + oldDue);
	}
	//tests of frequency change and sum of frequencies change need to be in this order
        //because if the frequency doesn't change, the sum of frequencies won't change
	if(p.getSumFrequency() == oldSumFreq) {
	    System.err.println("The sum of frequencies did not change from " + oldSumFreq);
	}
    }
    /* Test the specific bug that a second card isn't introduced after the 
       very first one is gotten correct. */
    public static void testKnewResponse1() {
	Proctor p = new Proctor(new HeapQList(additionList()));
	Card first = p.nextCard();
	if(p.getQList().lastActiveIndex() < 0) {
	    System.err.println("The first card was not make active when it was shown.");
	}
	p.knewResponse();
	Card second = p.nextCard();
	if(p.getQList().lastActiveIndex() == 0) {
	    System.err.println("A second card was not introduced when the first card was gotten correct.");
	}
	if(first == second) {
	    System.err.println("The second card was not shown despite that it was new.");
	}
    }
}
