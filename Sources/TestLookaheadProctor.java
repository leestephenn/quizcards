import java.util.*;
import quizcards.*;
public class TestLookaheadProctor {
    public static void main(String[] args) {
	System.out.println("Test of DueHeap sorter");
	testDueHeapSorter();
    }
    
    public static void testDueHeapSorter() {
	PriorityQueue<Card> heap = new PriorityQueue<Card>(11, (new LookaheadProctor()).dueHeapSorter());
	for(int i=0; i < 12; i++) {
	    Card c = new Card("What is 1 + " + i + "?", (1+i) + ".");
	    c.setFrequency((12.0 - i)/13.0);//set the cards frequencies in reverse order
	    heap.add(c);
	}
	Iterator<Card> it = heap.iterator();
	Card last = it.next();
	while(it.hasNext()) {
	    Card c = it.next();
	    if(!(last.getFrequency() < c.getFrequency())) {
		System.err.println("The frequency of " + c + " was not greater " + 
			"than the frequency of the preceding card.");
	    }
	}
    }
}
