/* Stores data from DryRunProctor */
public class CardChange {
    private int turn;
    private boolean known;
    private double oldFrequency;
    private double newFrequency;
    private double oldExponent;
    private double newExponent;

    public CardChange(int aTurn, double oldFreq, double oldExp) {
	//I'm not putting in any validation code because I'm the only one who uses this
	turn = aTurn;
	oldFrequency = oldFreq;
	oldExponent = oldExp;
    }

    public void setNewValues(boolean known, double newFreq, double newExp) {
	this.known = known;
	newFrequency = newFreq;
	newExponent = newExp;
    }
    public double frequencyChange() {
	return newFrequency - oldFrequency;
    }
    public double exponentChange() {
	return newExponent - oldExponent;
    }
    public double getOldFrequency() {
	return oldFrequency;
    }
    public double getNewFrequency() {
	return newFrequency;
    }
    public double getOldExponent() {
	return oldExponent;
    }
    public double getNewExponent() {
	return newExponent;
    }
    public String toString() {
	String turnString = "Turn: " + turn;
	String knownString = ", Known: " + known;
	String freqString = ", Old Frequency: " + oldFrequency + ", Its change: " + frequencyChange();
	String expString = ", Old Exponent: " + oldExponent + ", Its change: " + exponentChange();
	return turnString + knownString + freqString + expString;
    }
}
