# quizcards

This is a flash card program I wrote as an extended high school project. All work I know of is from 2010, but the project spanned years.

The program tries to present cards to the user "intelligently", based on feedback from the user whether they know the answer.

If you actually want a mature flash card program with many of the same goals, please the [The Mnemosyne Project](https://mnemosyne-proj.org/). (I was unaware of it when I worked on this project.) This code is primarily posted to demonstrate work, not because I expect anyone to actually use it. However, it does compile and run.

## Building

You can build this project by running

```shell
mkdir Classes
javac -d Classes/ Sources/quizcards/*.java
```

To re-build, you only need to run the second command.

## Running

To run this project, run

```shell
java -cp Classes/ quizcards.JarStarter
```

(As that line suggests, this project was at one point packaged into an executable JAR file.)

On the dialog that should pop up, click the button labeled "Start Proctor". Open one of the files in the SavedQLists directory in this repository. You should then get to answer some questions. Once you are finished, save the deck to keep track of your progress learning the flash cards.

The other option on the initial dialog, "Start QuizCardBuild", is for building new decks of flash cards.

## Code structure

The actual code for the program is in Sources/quizcards. The classes immediately in Sources/ are test files.
